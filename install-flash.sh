#!/bin/bash

#_______________________________________________________________________
#
# SELECTION DE FLASH A UTILISER
#_______________________________________________________________________
flashInstallMode="manuel"

if [ $# == 1 ] 
then
    if [ ! "$1" == "post" ]
    then
        exit 1
    else
        flashInstallMode="post"
    fi
fi

SD_install_flash=$(dirname $(readlink -f $0))/


flashType=$(
zenity --list \
--width=400 \
--height=600 \
--title "Sélection du type d'installation pour flashplayer" \
--text "
  <b>Adobe:</b> version 11.2, développement arrêté , fonctionnel, pour les sites ne nécessitant pas une version plus récente
  
  <b>Pipelight :</b> Utilisation du plugin flash pour Windows via wine
        - <span color=\"red\">Utilise une source externe : ppa:pipelight/stable</span>
        - Peut être nécéssaire pour visionner certains site de replay
        - Risques d'instabilités
  
  Les utilisateurs avertis pourront tester également :
  
    <b>Gnash+Lightspark :</b> Implémentation libre du plugin, 
        Gnash pour les versions, anciennes
        Lightspark pour les versions récentes
        
    <b>Shumway :</b> Plugin fourni par Mozilla, développement récent
    
<b>Les utilisateurs voulant profiter de sites de rediffusion pourront s'orienter vers chromium-browser + pepperflashplugin-nonfree</b>" \
--radiolist \
--hide-column=3 \
--column "Pick" --column "options"          --column="id" \
        'TRUE'            "Adobe"             "adobe"     \
        'FALSE'           "pipelight"         "pipelight" \
        'FALSE'           "Gnash+Lightspark"  "gnu"       \
        'FALSE'           "Shumway"           "shumway"   \
        --print-column=3 \
);
exitZenity=$?

if [ $exitZenity  != 0 ]
then
zenity --warning --text "Annulation lors de la sélection de la version de flash"
exit 1
fi

if [ ! -z "$flashType" ]
then
    ans="$ans --flash $flashType"

    if [ "$flashInstallMode" == "manuel" ]
    then
        xterm -e "echo Installation de flash $flashType ; sudo python3 ressources/install_flash.py --install $flashType;echo -e '======\n FIN \n=====';$BASH"
    fi

fi

