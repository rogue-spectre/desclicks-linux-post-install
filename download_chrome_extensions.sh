#!/bin/bash

SD_chrome_extensions=$(dirname $(readlink -f $0))/

crx=$(
zenity --list                                                        \
--width=400 \
--height=400 \
--title "Sélection des extensions pour le navigateur Chromium" \
--text "
L'installation des extensions est faire manuellement avec les fichiers téléchargés.
Pour ce faire  rendez vous dans la section Paramètres \> Extensions de
Chromium puis faites un glisser-déposer des extensions

- <span color=\"red\">La souscription aux listes de blocage est a faire manuellement
  pour adblock plus pour le moment</span>
"                                                                    \
--checklist                                                          \
--hide-column=3                                                      \
--column    "Pick" --column "options"     --column "id"              \
            'TRUE'          "Bloqueur de publicités : Adblock Plus"   "adblockplus"      \
            'TRUE'          "Forcer https : https everywhere"     "httpsEverywhere"     \
            --print-column=3 \
            --separator=" ");

exitZenity=$?

if  [ $exitZenity != 0 ] 
then
zenity --warning --text "Annulation lors de la sélection d'extensions pour Chromium"
exit 1
fi

if [ -z "$crx" ] 
then
    exit 0
    #ans="$ans --crx $crx"
fi

version=`chromium-browser --version 2>/dev/null`
if [ -z "$version" ] 
then
    echo "[ WARNING ] impossible de déterminer la version de chromium-browser
                      la valeur par défaut $version_default sera utilisée "
    version="39.0.2171.65"
fi

lang=fr-FR

OIFS=$IFS
IFS=' '
arr2=$IN
dir_crx_save="${SD_chrome_extensions}CHROME_EXTENSIONS/"
mkdir "${dir_crx_save}"
for ext in $crx
do
    echo $ext
    case $ext in
    "adblockplus" )
        ID_EXT="cfhdojbkjhnklbpkdaibdccddilifddb"
        ;;
    "httpsEverywhere" )
        ID_EXT="gcbommkclmclpchllfjekcdonpmejbdp"
        ;;
    * )
        echo "[ ERROR ] extension non prise en charge"
        exit 1
        ;;
    esac
    
    wget --timeout=20 -O ${dir_crx_save}${ext}.crx "http://clients2.google.com/service/update2/crx?response=redirect&x=id%3D${ID_EXT}%26uc%26lang%3D${lang}&prod=chrome&prodversion=${version}"
done
exit 0
