
Notes de version :

-------

   - Ajout possibilité installation 
	- teamviewer
        - gitso

   - Script téléchargement d'adblock plus / https everywhere pour chromim

02-11-2014

   - Isolation des parties d'installation des logiciels non-libres + flash

   - Amélioration des message zenity
	ttf-mscore-fonts
   
   - Adblock Plus

   - Regeneration des paneaux si souhaite avec raccourcis

   - desktop :
      - montage des periphériques amovibles sur le bureau
      - blacklistage possible du lecteur de disquettes
      
   - Fond d'écran démarrage simplifié ( espoir de gain en temps de démarrage )

   - configs :
	gimp : mode fenetre unique
	... d'autres a venir

   - logos :
	- modif logo custom xfce par défaut, en accord avec thème faenza
	- possibilité modif logo par installateur

   - Ajout recréation tableaux de bord
	script ressources/panels.sh

   - flash :
	+ ajout type de version flash à utiliser, normal, shumway, pipelight, gnash-lightspark
        + chromium + pepperflash

   - skel :
	ajout dossiers ressources/etc/skel de fichiers de configurations à copier

   - numpad :
        + desactivation de la sauvegarde d'état du numpad (xfce), si numlockx activé pour lightdm

   - lightdm :
	+ desactivation session invite
   
   - firefox/thunderbird/libreoffice :
	+ ajout dico fr
        + option java plugin

   - unity 
      - suppr tous scopes sauf vitaux
      - suppr facebook scopes

   - comptes en ligne
	
   - extras non free non recommendés 
	- googleearth
	- skype


09-06-14
   - Amélioration du lanceur
	- exécution par clic possible
        - exécution depuis un autre dossier
        
   - fix adblock edge preference non prises en compte
   
   - Extension firefox incluses :
	- Adblock edge
        - httpsEverywhere
        - NoScript

   - preparation pour prise en compte du bureau unity ( rien de mis en oeuvre coté utilisateur encore )
