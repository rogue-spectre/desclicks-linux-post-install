#!/bin/bash

#_______________________________________________________________________
#
# SELECTION DES LOGICIELS EXTRAS NON-LIBRES 
#_______________________________________________________________________ 
extraNonFreeInstallMode="manuel"

if [ $# == 1 ] 
then
    if [ ! "$1" == "post" ]
    then
        exit 1
    else
        extraNonFreeInstallMode="post"
    fi
fi

SD_install_extraNonFree=$(dirname $(readlink -f $0))/

extras=$(
zenity --list \
--height=650 \
--width=400 \
--title "Sélection des logiciels non libres - non-recommandés" \
--text "
    <span color=\"red\">La sélection de logiciels dans cette liste active automatiquement l'utilisation
    des dépôts partenaires</span>
    
    <b>Acrobat Reader XI:</b> Permet de lire/remplir les formulaire CERFA, <span color=\"red\">version wine</span>
    
    <b>Acrobat Reader 9.5:</b> Obsolète, mais peut permettre de lire/remplir les formulaire CERFA
       <i>Alternatives</i> : evince, okular (kde)
    
    <b>GoogleEarth :</b> avec les vignettes panoramio actives... au dernier test
        <span color=\"red\">Installe ttf-mscorefonts</span>
        <i>Alternative</i> : marble + kde-l10n-fr (kde), marble-qt (en anglais)
        
    <b>Skype :</b> + quelques fix d'indicateur dans la barre des taches, ( active les dépôts partenaires )
        <i>Alternative</i> : hello ( firefox 34+ ), ekiga
            
    <b>Teamviewer</b> :
            - Version Aide est suffisante pour une demande de dépannage. Celle ci est installé dans /opt
              et nécéssite que l'utilisateur fasse partie du groupe teamviewer. L'utilisateur actuel
              sera ajouté automatiquement à ce groupe.
        <i>Alternative</i> : logiciels basés sur vnc, gitso (cfg à faire côté assistant)
    " \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                         --column "id"               \
            'FALSE'         "Acrobat Reader XI"                                       "acroread11"       \
            'FALSE'         "Acrobat Reader 9.5.5 Anglais"                            "acroread"         \
            'FALSE'         "GoogleEarth"                                             "googleearth"      \
            'FALSE'         "Skype"                                                   "skype"            \
            'FALSE'         "Teamviewer Complet"                                      "teamviewer"       \
            'FALSE'         "Teamviewer Aide"                                         "teamviewer_qs"     \
            --print-column=3 \
            --separator=" ");
exitZenity=$?

#    
#                

#
#http://www.skype.com/fr/download-skype/skype-for-linux/downloading/?type=dynamic
#download.teamviewer.com/download/teamviewer_linux.deb
#download.teamviewer.com/download/teamviewer_qs.tar.gz
if [ $exitZenity  != 0 ]
then
zenity --warning --text "Annulation lors de la sélection des logiciels extras non-libres"
exit 1
fi

if [ ! -z "$extras" ]
then
    ans="$ans --deb-partner --extra-non-free $extras"

    if [ "$extraNonFreeInstallMode" == "manuel" ]
    then
        cd ressources
        xterm -e "echo Installation de $extras ; sudo python3 install_extra_nonfree.py --install $extras;echo -e '======\n FIN \n=====';$BASH"
    fi

fi
