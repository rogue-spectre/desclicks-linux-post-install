#!/bin/bash
#
# Interface graphique presentant les options de post installation
# d'une distribution ubuntu/xubuntu
#
#
# recuperation du code de retour de zenity "
# "0" Ok
# "1" Fermeture ou Annulation du script

# recuperation de dossier d'execution du script
SD_post_custom=$(dirname $(readlink -f $0))/

cd ${SD}ressources

# initialisations
unset __install__
unset __remove__
unset ans
unset ans_user


autoargs=''

which xterm > /dev/null
return_xterm=$?

echo "installation de xterm" && sudo apt-get update && sudo apt-get install -y xterm

which aptitude > /dev/null
return_aptitude=$?

if [ ! $return_aptitude -eq 0 ] 
then
    xterm -e "echo [ INFO ] Installation du logiciel aptitude et xterm nécessaire ; sudo apt-get update && sudo apt-get install -y aptitude"
fi

which aptitude > /dev/null
return=$?

if [ ! $return -eq 0 ]
then
    zenity --error --text "Échec de l'installation d'aptitude"
    exit 1
fi



#_______________________________________________________________________
#
# FONCTION
#_______________________________________________________________________

isOnlineAccountInit=false
#rm -rf isOnlineAccountInit

#
# Recherche d'informations sur le centre de controle en ligne et ses plugins
#
function onlineAccountInit()
{
    rm -rf OnlineAccountCenterInstalled
    if `aptitude search unity-control-center-signon | grep -q ^i`
    then
        touch OnlineAccountCenterInstalled
    fi
    #else
    #    isOnlineAccountCenterInstalled=false
    #fi
    
    aptitude search ^account-plugin > onlineAccountpluginsRawList
    #onlineAccountPluginsToRemove=$( cat onlineAccountpluginsRawList | grep    ^i |sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2)
    #onlineAccountPluginsList=$(     cat onlineAccountpluginsRawList |             sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
    #onlineAccountPluginsInstalled=$(cat onlineAccountpluginsRawList | grep    ^i |sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
    #onlineAccountPluginsAvailable=$(cat onlineAccountpluginsRawList | grep -v ^i |sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)

    isOnlineAccountInit=true
    #touch isOnlineAccountInit
}


#onlineAccountpluginsToRemove=$(aptitude search ^account-plugin | grep    ^i |sed -e 's/ \s*/ /' | cut -d ' ' -f 2)
#onlineAccountsPluginsList=$(aptitude search ^account-plugin |             sed -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
#onlineAccountsPluginsInstalled=$(aptitude search ^account-plugin | grep    ^i |sed -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
#onlineAccountsPluginsAvailable=$(aptitude search ^account-plugin | grep -v ^i |sed -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)

#
# Message de vérification avant lancement du script 
#
function checkAndInstall()
{
    FILE=message_confirm_before_launch

    echo "Maintenant on va :

    - Exécuter ( votre mot de passe sera requis ):
      Si bureau xfce :
        ./waitPanels.sh $panelcfg &
      Si bureau unity :
        ./fixubuntu.sh

      puis :
        python3 custom_usr.py --desktop $desktop $ans_user
        sudo python3 custom_root.py --install $__install__ --skel $ans

    Les fichiers/dossiers suivants seront supprimés :
        ~/.config/xfce4/panel/whiskermenu*.rc
        ~/.config/libreoffice
        ~/.mozilla

    Le script last_custom.sh à été mis à jour à la racine du dossier. Il vous 
    permet de relancer la même customisation sur une autre machine sans 
    répondre aux questions.

    Redémarrez la machine en fin d'installation.
    " > $FILE

    sed -e 's/--/\\\n                --/g' -i $FILE

    #zenity --info \
    #--width=400 \
    #--text \
    #"Maintenant on va :
    #
    #- Exécuter ( votre mot de passe sera requis ):
    #
    #    sudo python3 custom_root.py --install --skel $ans
    #    python3 custom_usr.py#
    #
    #Les fichiers/dossiers suivants seront supprimés :
    #    ~/.config/xfce4/panel/whiskermenu*.rc
    #    ~/.mozilla
    #    
    #Pensez à redémarrer la machine en fin d'installation
    #"
    zenity --text-info \
      --width=700 \
      --height=750 \
      --title=$FILE \
      --filename=$FILE

    exitZenity=$?

    if  [ "$exitZenity" != 0 ] 
    then
    zenity --warning --text "Annulation phase vérif"
    exit 1
    fi

    echo $desktop

    # Exécution des scripts
    myCustom=${SD_post_custom}last_custom_install.sh
    rm -rf $myCustom
    echo "#!/bin/bash" > $myCustom
    echo "cd ressources" >> $myCustom
    case "$desktop" in 
        'xfce')
            # lancement du processus d'attende de création des tableaux de bord xfce
            echo "python3 custom_usr.py --desktop xfce $ans_user ;xfce4-panel -r" >> $myCustom
            echo "./waitPanels.sh $panelcfg &" >> $myCustom
            echo "sudo python3 custom_root.py --install $__install__ --skel $ans" >> $myCustom
            echo "python3 custom_usr.py --desktop xfce $ans_user ;xfce4-panel -r" >> $myCustom
           
            python3 custom_usr.py --desktop xfce $ans_user
            xfce4-panel -r
            ./waitPanels.sh $panelcfg &
            xterm -e "echo -e '=========\n Début Post Installation Script \n=========\n';sudo python3 custom_root.py --install $__install__ --skel $ans; python3 custom_usr.py --desktop xfce $ans_user;xfce4-panel -r;echo -e '======\n FIN \n=====';$BASH"
        ;;
        'unity')
            if `echo "$ans" | grep -q "fixubuntu"`
            then
                echo "./fixubuntu.sh" >> $myCustom            
                xterm -e "echo -e '=========\n FIX PRIVACY UBUNTU \n========\n';./fixubuntu.sh"
            fi
            echo "python3 custom_usr.py $ans user --desktop unity" >> $myCustom
            echo "sudo python3 custom_root.py --install $__install__ --skel $ans" >> $myCustom
            echo "python3 custom_usr.py $ans user --desktop unity" >> $myCustom
            
            python3 custom_usr.py --desktop unity 
            xterm -e "echo -e '=========\n Début Post Installation Script \n=========\n';sudo python3 custom_root.py --install $__install__ --skel $ans; python3 custom_usr.py --desktop unity;echo -e '======\n FIN \n=====';$BASH"
        ;;
        *)
            echo "sudo python3 custom_root.py --install $__install__ --skel $ans" >> $myCustom
            xterm -e "echo -e '=========\n Début Post Installation Script \n=========\n';sudo python3 custom_root.py --install $__install__ --skel $ans;echo -e '======\n FIN \n=====';$BASH"
        ;;
    esac
    chmod +x $myCustom

    # Fin
    #zenity --info \
    #--height=100 \
    #--text 

    FILE=message_end
    zenity --text-info \
      --width=500 \
      --height=400 \
      --title=$FILE \
      --filename=$FILE
}   
#_______________________________________________________________________
#
# BONJOUR
#_______________________________________________________________________

hello=$(
zenity --list \
--title "Bienvenue" \
--width=500         \
--height=450         \
--text "
Bienvenue dans le script de Post Installation pour :
<span color=\"green\">Ubuntu ou Xubuntu 14.04</span>.

Ce script est destiné plus particulièrement à une installation francophone. 

<span color=\"red\"> ATTENTION ce script a été testé uniquement sur Ubuntu et Xubuntu 14.04, 
son utilisation sur une autre distribution peu être hasardeuse.</span>

L'installation de logiciels portant la mention \"(kde)\" est à réserver aux utilisateurs avertis.

Les remarques peuvent être directement adressées à :
  <span color=\"green\">anthony@desclicks.net</span>
 
Votre mot de passe vous sera demandé en fin d'éxécution" \
--radiolist \
--hide-column=3 \
--column   "Pick" --column "options"   --column "id" \
            'TRUE'  "Choix manuel des options"                               "manuel"       \
            'FALSE' "Configuration automatique pour utilisateurs avertis"    "auto"        \
            --print-column=3 \
);
exitZenity=$?

if [ $exitZenity  != 0 ] || [ -z "$hello" ]
then
zenity --warning --text "Annulation lors de la présentation"
exit 1
fi

#_______________________________________________________________________
#
# FULL AUTO
#_______________________________________________________________________

# essai de detection du bureau
flagxfce=FALSE
flagunity=FALSE
flagmate=FALSE
flagnodesktop=TRUE

if [ ! -z $DESKTOP_SESSION ]
then
    auto_install="ttf-mscorefonts-installer rar gthumb shotwell"
    auto_remove="abiword abiword-plugin-grammar abiword-plugin-mathview gnumeric parole gmusicbrowser xfburn ristretto"
    case $DESKTOP_SESSION in 
        'xubuntu'|'xfce')
            flagxfce=TRUE
            flagnodesktop=FALSE
            autoargs="$autoargs --panel"
            desktop='xfce'
            ;;
        'mate')
            flagmate=TRUE
            flagnodesktop=FALSE
            desktop='mate'
            ;;
        'ubuntu')
            flagunity=TRUE
            flagnodesktop=FALSE
            desktop='unity'
            autoargs="$autoargs --fixubuntu"
            ;;
        *)
            flagnodesktop=TRUE
            desktop='none'
            ;;
    esac
    autoargs="--rmmod floppy mei_me --desktop $desktop --pdf-hello --no-session-save --lightdm noguest  --light-locker off --libreoffice orthographe  --flash adobe --dvd --xpi adb  httpsEverywhere --selection bureau education multimedia infographie utils jeux --patch eject "
fi

if [ $hello == "auto" ]
then
    ans="$autoargs  "
    __install__="$auto_install"
    __remove__="$auto_remove"
    checkAndInstall
    exit 0
fi

#_______________________________________________________________________
#
# SELECTION DU BUREAU
#_______________________________________________________________________

desktop=$(
zenity --list \
--width=400 \
--height=500 \
--title "Choix du Bureau" \
--text  "
  Sélectionnez le bureau correspondant à celui déjà installé sur  votre système 
  afin d'accéder aux options de personnalisation adéquates.
    
    <b>mate :</b> traductions panneau config

    <b>xfce :</b> améliore l'apparence, les traductions, et quelques configurations de logiciels
    La configuration est appliquée à l'utilisateur courant ainsi qu'aux prochains utilisateurs créés.

    <b>unity :</b> corrige les problèmes 'Amazon' et 'Recherches en ligne dans le dash'.
     Actuellement il faut relancer le script ressources/fixubuntu.sh pour les autres utilisateurs.
     + installation de unity-tweak-tool pour faciliter la personnalisation de l'interface par l'utilisateur.

    <b>Pas de configuration spécifique :</b> Aucune personnalisation d'apparence, ni d'amélioration
        spécifique au bureau ne sera réalisée.
" \
--radiolist \
--hide-column=3 \
--column "Pick" --column "options"                          --column "id" \
        $flagmate            "mate"                              "mate"      \
        $flagxfce            "xfce (xubuntu)"                    "xfce"      \
        $flagunity           "unity (ubuntu)"                    "unity"     \
        $flagnodesktop       "Pas de configuration spécifique"   "none"      \
        --print-column=3 \
);
exitZenity=$?

if [ $exitZenity  != 0 ] || [ -z "$desktop" ]
then
zenity --warning --text "Annulation lors de la sélection du Bureau"
exit 1
fi

if [ ! -z "$desktop" ]
then
    ans="$ans --desktop $desktop"
fi


#_______________________________________________________________________
#
# CONFIGURATION SPECIFIQUE POUR CHAQUE BUREAU
#_______________________________________________________________________

# utilisation de mint update sous mint
USE_MINTUPDATE=0
if `lsb_release --id | grep -qi "mint"`
then
    zenity --width=500 --question \
    --ok-label "Mise à jour classique pour la post-install" \
    --cancel-label "Utiliser MintUPdate pour mettre à jour manuellement" \
    --text "LinuxMint propose son gestionnaire de mises à jour MintUpdate permettant de temporiser certaines mises à jour. \
Ce gestionnaire vise à vous proposer un système plus stable aux détriment de  nouvelles fonctionnalités."

    USE_MINTUPDATE=$?
fi

if [ ! ${USE_MINTUPDATE} == 0 ]
then
    ans="$ans --use-mintupdate"
fi

#_______________________________________________________________________
#
# CONFIGURATION SPECIFIQUE POUR CHAQUE BUREAU
#_______________________________________________________________________
case $desktop in 
    'xfce')
    desktopcfg=$(
    zenity --list \
--width=400 \
--height=400 \
--title "Configuration spécifique pour XFCE" \
--text "
    <b>Suppression sauvegarde session : </b>
            Supprime la possibilité de restaurer l'ensemble des programmes restés ouverts à 
            l'extinction précédente, ce qui améliore la vitesse et la stabilité de démarrage.

    <b>Logo installeur : </b>remplace le logo xfce du menu par ./ressources/pixmaps/custom-installeur.png
 
    <b>Désactiver le verrouillage d'écran :</b>
            Le nouveau gestionnaire de verouillage light-locker présume une utilisation du pc dans un lieu
            public et coupe les applications sonores lors d'un verrouillage de l'écran. Désactiver
            le verrouillage permet notamment d'écouter sa musique sur son pc en faisant une autre activité.
" \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                       --column "id"                  \
            'TRUE'          "Suppression sauvegarde session"               "no-session-save"              \
            'FALSE'         "Logo installeur"                              "logo custom-installeur.png"   \
            'TRUE'          "Désactiver le vérouillage d'écran"            "light-locker off"             \
--print-column=3 \
--separator=" --");
    exitZenity=$?
    if [ $exitZenity  != 0 ] 
    then
        zenity --warning --text "Annulation lors de la configuration spécifique à chaque xfce"
    exit 1
    fi
    panelcfg=$(
     zenity --list \
--width=400 \
--height=500 \
--title "Configuration des tableaux de bord XFCE" \
--text "
  Sélectionner l'option de personnalisation des tableaux de bord d'xfce ( barre des tâches / lanceurs ). 
  Une capture d'écran 'preview_xfce_bureau.png' est disponible à la racine du dossier de ce script.

    <b> Barre des tâches : </b>
        + Menu des applications ( whisker ) avec logo xfce personnalisé et Mention 'Menu',
        + Lanceurs optionnels,
        + Boutons d'actions de vérouillage et fermeture à droite
        
    <b> Barre de lanceurs : </b>
        Crée un tableau de bord, placé en bas de l'écran pour les raccourcis applicatifs
" \
--radiolist \
--hide-column=3 \
--column   "Pick" --column "options"                                                  --column "id" \
            'TRUE'          "Barre des tâches + Barre de lanceurs "                   "3"           \
            'FALSE'         "Barre des tâches avec lanceurs + Barre de lanceurs"      "3 0"         \
            'FALSE'         "Garder la disposition actuelle"                          "0"           \
--print-column=3 \
--separator="");   
exitZenity=$?
if [ $exitZenity  != 0 ] 
then
    zenity --warning --text "Annulation lors de la configuration spécifique à chaque tableau de bord"
exit 1
fi

if [ ! -z "$panelcfg" ]
then
    if [  "$panelcfg" != "0" ]
    then
        ans="$ans --panel"
    fi
fi
    ;;
    'unity')
    desktopcfg=$(
    zenity --list \
--width=400 \
--height=300 \
--title "Configuration spécifique pour Unity" \
--text "
    <b> Fix Ubuntu : </b> désactive les recherche en lignes dans le dash pour les suggestion commerciales
                   via le script fixubuntu.sh <span color=\"green\">https://fixubuntu.com/</span>
                   et supprime les scopes non nécessaires.
                + suppression lanceur amazon
" \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                       --column "id"                  \
            'TRUE'          "Fix Ubuntu Privacy "                           "fixubuntu"                   \
--print-column=3 \
--separator=" --");
    exitZenity=$?
    if [ $exitZenity  != 0 ] 
    then
        zenity --warning --text "Annulation lors de la configuration spécifique à chaque unity"
    exit 1
    fi
    
    #source ${SD_post_custom}ressources/online_account.sh
    ;;
  
esac

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la configuration spécifique à chaque bureau"
exit 1
fi

if [ ! -z "$desktopcfg" ]
then
    ans="$ans --$desktopcfg"
fi




#_______________________________________________________________________
#
# CONFIGURATIONS COMMUNES AUX BUREAUX
#_______________________________________________________________________

cfg=$(
zenity --list \
--title "Configurations communes" \
--text  "Sélectionner les paramètres" \
--height=400 \
--width=400 \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                              --column "id"                      \
            'TRUE'          "Désactivation session invite"                                 "lightdm noguest"         \
            'FALSE'         "Activation pave numérique au démarrage"                       "numpad"                  \
            'TRUE'          "PDF de bienvenue sur le bureau"                               "pdf-hello"               \
            'TRUE'          "Correction Orth/Gramm libreoffice (java requis)"              "libreoffice orthographe" \
            'FALSE'         "Activer les dépôts partenaires"                               "deb-partner"             \
            'TRUE'          "Configuration APT, maj hebdomadaire, maj securité auto"       "apt"                     \
            'TRUE'          "Swp reste inactif tant qu'il reste plus de 5% de ram libre"   "swap 5"                  \
            'TRUE'          "Masquer lecteur de disquettes"                                "rmmod floppy"            \
            --print-column=3 \
            --separator=" --");
exitZenity=$?

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la configuration commune aux bureaux"
exit 1
fi

if [ ! -z "$cfg" ]
then
#    if `echo $cfg | grep -q lightdm`
#    then
#        cfg=`echo $cfg | sed -e 's/--lightdm_//g' -e 's/lightdm_//g'`
#        cfg="lightdm $cfg"
#    fi
    ans="$ans --$cfg"
    if `echo $ans | grep -q "rmmod floppy"`
    then
        ans="$ans mei_me"
    else
        ans="$ans --rmmod floppy"
    fi
fi
 
#_______________________________________________________________________
#
# PATCH BUGS
#_______________________________________________________________________
 
 patchs=$(
zenity --list \
--title "Patch Bugs" \
--text  "<b>Merci de vérifier que les bugs ne sont pas déjà résolu en amont avant d'appliquer le patch correspondant</b>
         Sélectionner les paramètres" \
--height=400 \
--width=400 \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                              --column "id"            \
        'TRUE'          "Forcer démontage CD/DVD lors de l'éjection manuelle"          "eject"             \
        'TRUE'          "Fix pour changement de mot de passe (xfce/mate)"              "users-admin"       \
  --print-column=3 \
            --separator=" ");
exitZenity=$?

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la selection des patchs"
exit 1
fi

if [ ! -z "$patchs" ]
then
ans="$ans --patchs $patchs"
fi

#_______________________________________________________________________
#
# SELECTION DES LOGICIELS
#_______________________________________________________________________ 

if [ $desktop == 'xfce' ]
then

removeSofts=$(
zenity --list \
--title "Suppression de logiciels" \
--text  "Pour chacun des logiciels à supprimer, 
une alternative sera proposée par la suite :

  <b>Abiword, Gnumeric</b> > <i>suite LibreOffice</i>
  <b>Parole,gmusicbrowser</b> > <i>VLC</i>
  <b>xfburn</b> > <i>brasero</i>
  " \
--height=400 \
--width=400 \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                   --column "id"                                             \
            'TRUE'         "Abiword"                   "abiword abiword-plugin-grammar abiword-plugin-mathview"  \
            'TRUE'         "Gnumeric"                  "gnumeric"                                                \
            'TRUE'         "Parole"                    "parole"                                                  \
            'TRUE'         "gmusicbrowser"             "gmusicbrowser"                                           \
            'TRUE'         "xfburn"                    "xfburn"                                                  \
            --print-column=3 \
            --separator=" ");
exitZenity=$?

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la suppression des logiciels"
exit 1
fi

if [ ! -z "$removeSofts" ]
then
    ans="$ans --remove $removeSofts"
fi

fi

selectSoftsFree=$(
zenity --list \
--height=600 \
--width=400 \
--title "Sélection de logiciels libres à Installer" \
--text "
    <b>Apprentissage :</b> klavaro (dactylo)
    <b>Bureautique :</b>
            libreoffice + integration gtk
            pdfmod : manipulation de pdf
            verbiste : conjugaison
            gnome-font-viewer : gérer les polices
    <b>Multimedia  :</b> vlc, sound-juicer
    <b>Infographie :</b> gimp, mypaint
    <b>Outils      :</b> gksu, p7zip-full, unzip, gparted, gnome-disk-utility, baobab
    <b>Jeux        :</b> tanglet, gnome-mahjong, gnome-chess, aisleriot
    <b>Jeunes tux  :</b> tuxpaint, tuxmath, supertuxkart, tuxtype
    <b>Meta-Éducatifs :</b>
        ubuntu-edu-preschool ubuntu-edu-primary, ubuntu-edu-secondary, ubuntu-edu-tertiary
    " \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                         --column "id"                \
            'TRUE'          "Bureautique"                                             "bureau"            \
            'TRUE'          "Apprentissage"                                           "education"         \
            'TRUE'          "Multimedia"                                              "multimedia"        \
            'TRUE'          "Infographie"                                             "infographie"       \
            'TRUE'          "Outils"                                                  "utils"             \
            'TRUE'          "Jeux"                                                    "jeux"              \
            'FALSE'         "Jeunes tux"                                              "tux"               \
            'FALSE'         "Méta Educatifs ( dependances kde ) "                     "edu"               \
--print-column=3 \
--separator=" "
);
exitZenity=$?

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors du choix de sélection de logiciels libres"
exit 1
fi

if [ ! -z "$selectSoftsFree" ]
then
    ans="$ans --selection $selectSoftsFree"
fi

softsFree=$(\
zenity --list \
--height=600 \
--width=400 \
--title "Sélection des logiciels libres" \
--text "
  Choix de logiciels libres à installer
    
  Shotwell est installé via le dépot stable du développeur, permet la publication via picasaweb
    " \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                --column "id"                 \
           'TRUE'          "brasero : gravure"                                "brasero"           \
           'TRUE'          "gthumb : visionneuse d'image"                     "gthumb"            \
           'FALSE'         "Chromium : Navigateur Web avec flash player"      "chromium-browser chromium-browser-l10n pepperflashplugin-nonfree" \
           'FALSE'         "pitivi : montage vidéo"                           "pitivi"            \
           'FALSE'         "shotwell : gérer photos"                          "shotwell"          \
           'FALSE'         "nemo : gestionnaire de fichier"                   "nemo"              \
           'FALSE'         "scratch : apprentissage programmation"            "scratch"           \
           'FALSE'         "geogebra : logiciels apprentissage géométrie"     "geogebra"          \
           'FALSE'         "poedit : traduire les programmes"                 "poedit"            \
           'FALSE'         "texmaker : éditeur latex"                         "texmaker texlive texlive-lang-fr texlive-generic-extra texlive-math-extra"  \
           'TRUE'          "vim : éditeur de texte en ligne de commande"      "vim"               \
           'FALSE'         "emacs : éditeur de texte et code"                 "emacs"             \
           'FALSE'         "geany : éditeur de texte et code"                 "geany"             \
           'FALSE'         "gnuplot : tracer des graphiques"                  "gnuplot gnuplot-x11" \
           'FALSE'         "terminator : terminal alternatif"                 "terminator" \
           'FALSE'         "gitso : aide à distance facile"                   "gitso" \
--print-column=3 \
--separator=" " );
exitZenity=$?

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la sélection des logiciels"
exit 1
fi

if [ ! -z "$softsFree" ]
then
    __install__="$__install__ $softsFree"
fi

#_______________________________________________________________________
#
# Changement de logiciels par défaut / only for xfce
#_______________________________________________________________________

if [ $desktop == 'xfce' ]
then

prefered_apps=$(
zenity --list \
--title "Applications préférées" \
--text  "Sélectionner les paramètres" \
--height=300 \
--width=400 \
--text " 
VLC     : lecteur vidéo/audio complèt
brasero : gravure cd/dvd
" \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"    \
            'TRUE'          "vlc"       \
            'TRUE'          "brasero"   \
            --print-column=2 \
            --separator=" "
)

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la selection d'applications par défaut"
exit 1
fi


if [ ! -z "$prefered_apps" ]
then
    ans_user="$ans_user --prefered-apps $prefered_apps"
fi

fi

#_______________________________________________________________________
#
# Logiciels non libres
#_______________________________________________________________________

softsNotFree=$(\
zenity --list \
--height=450 \
--width=400 \
--title "Sélection des logiciels non libres" \
--text "
<span color=\"red\">En choisissant l'installation d'un paquet ci-dessous vous acceptez implicitement les licences y étant attachées</span>

    <b> Playonlinux : </b>
        facilite l'installation de certains logiciels windows, notamment Microsoft office
        installe également les polices microsoft.
    <b> Flash : </b> permet la lecture d'animations flash sur votre navigateur, le choix de version est proposé ensuite
    <b> Lecture CD/DVD :</b>  pour des raisons de licences les librairies permettant de lire les supports protégés du commerce
                       ne sont pas incluses par défaut, cette option résoud ce manque.
    " \
--checklist \
--hide-column=3 \
--column   "Pick" --column "options"                                                                                           --column "id"                 \
            'TRUE'          "Polices Microsoft : (licence Eula)"                                                                "ttf-mscorefonts-installer"  \
            'TRUE'          "rar : format compression"                                                                          "rar"                        \
            'FALSE'         "Playonlinux "                                                                                      "playonlinux"                \
            'TRUE'          "Flash plugin"                                                                                      "__flash"                    \
            'TRUE'          "Lecture CD/DVD du commerce"                                                                        "__dvd"                      \
--print-column=3 \
            --separator=" ");
            
exitZenity=$?

if [ $exitZenity  != 0 ] 
then
zenity --warning --text "Annulation lors de la sélection des logiciels non libres"
exit 1
fi    

flashType=none
if [ ! -z "$softsNotFree" ]
then
    for select in 'flash' 'dvd'
    do
        echo "$select"
        if `echo $softsNotFree | grep -q "__$select"`
        then
            softsNotFree=`echo $softsNotFree | sed -e "s/__$select//"`
            case "$select" in
            "flash")
                flashType=true
                ;;
            *)
             ans="$ans --$select"
                ;;
            esac
        fi
    done
    __install__="$__install__ $softsNotFree"
fi      



#if [ ! -z "$selectSoftsNotFree" ]
#then
#    ans="$ans --$selectSoftsNotFree"
#fi

#_______________________________________________________________________
#
# SELECTION DE FLASH A UTILISER
#_______________________________________________________________________

if [ "$flashType" == "true" ] 
then
    source ${SD_post_custom}install-flash.sh post
fi
#_______________________________________________________________________
#
# SELECTION DES LOGICIELS EXTRAS NON-LIBRES 
#_______________________________________________________________________ 
# source ${SD_post_custom}install-extra-non-free.sh post



#_______________________________________________________________________
#
# TÉLÉCHARGEMENT D'EXTENSIONS POUR CHROMIUM
#_______________________________________________________________________

if `echo "$__install__" | grep -q 'chromium-browser'`
then
    ./download_chrome_extensions.sh
fi

#_______________________________________________________________________
#
# REGENERATION DES TABLEAUX DE BORD > directement dans script python
#_______________________________________________________________________

#./panels.sh $panelcfg

#zenity --question --title="Vérification" --text="Régéneration des tableaux de bord ok ?"
#if  [ $exitZenity != 0 ] 
#then
#zenity --warning --text "Annulation phase vérif"
#exit
#$else
#ans="$ans --panel"
#fi

#_______________________________________________________________________
#
# FINALISATION ET LANCEMENT DU SCRIPT DE POST_INSTALL
#_______________________________________________________________________
checkAndInstall
exit 0