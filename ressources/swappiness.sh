#!/bin/bash

usage(){
    echo "usage :
    
    $0 <per>
    
    where <per> represents the percentage of free ram triggerring swap usage
    "
}

if [ ! $# == 1 ]
then
    usage
    exit 1
fi

if [ "${1}" == '--help' ] || [ "${1}" == '-h' ]
then
    usage
    exit 0
fi

if ! `echo "${1}" | egrep -q '^[1-6]?[0-9]$'`
then
    echo '[ ERROR ] invalid range'
    exit 1
fi
    
# application des parametres en dur

if `grep -qe '^vm\.swappiness=' /etc/sysctl.conf`
then
    sed -e "s/vm.swappiness=.*/vm.swappiness=$1/" -i /etc/sysctl.conf
else
    echo "vm.swappiness=$1" >> /etc/sysctl.conf
fi

# application des parametres pour la session
sysctl vm.swappiness=${1}
swapoff -av && swapon -av || echo "fail to update turn off or on the swap"

exit 0
