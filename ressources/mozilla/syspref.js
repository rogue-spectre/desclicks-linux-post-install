// 
//  Firefox preferences
//  /etc/firefox/syspref.js
//
// General
// Startup page
pref("browser.startup.homepage", "data:text/plain,browser.startup.homepage=https://startpage.com/do/mypage.pl?prfh=lang_homepageEEEs/white/fra");
pref("browser.search.defaultenginename", "data:text/plain,browser.search.defaultenginename=Startpage HTTPS - Francais");
pref("browser.search.selectedEngine", "data:text/plain,browser.search.selectedEngine=Startpage HTTPS - Francais");
// Ask where to save
//pref("browser.download.lastDir", "/some/directory");
pref("browser.download.useDownloadDir", false);
// Privacy
//   Tracking
pref("privacy.donottrackheader.value", true);
pref("privacy.donottrackheader.enabled", true);
//   History
//   keep until firefox closed
pref("network.cookie.prefsMigated", 2);
// Advanced
//   Updates
pref("browser.search.update", false);
