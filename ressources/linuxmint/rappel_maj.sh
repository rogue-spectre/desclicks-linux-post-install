#!/bin/bash

# lancement en différé au démarrage de la session
sleep 6

#vérification date dernière utilisation d'apt
lastaptdate_file=$(grep "Start-Date" /var/log/apt/history.log | tail -n 1 | grep -o [2-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9])

#history.log est vide
if [ -z "${lastaptdate_file}" ]
then
	lastaptdate_file=$(/bin/ls --time-style=+%Y-%m-%d -lt /var/log/apt/history.log*.gz | awk '{print $6}' | head -n 1)
fi

lastaptstamp=0
if `echo $lastaptdate_file | grep -q ^[2-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]$`
then
   lastaptstamp=`date -d "${lastaptdate_file}" +%s`
fi

currentdatestamp=$(date +%s)
twomonth=5356800
diff=`echo "( ${lastaptstamp} + ${twomonth} ) - ${currentdatestamp}" | bc`
# Affichage de l'information si dernière utilisation d'apt trop ancienne (2 mois)
if [ "${diff}" -lt "0" ]
then
    zenity --question \
                --title        "Rappel de mises à jour" \
                --text         "Prenez quelques minutes pour mettre à jour votre système" \
                --cancel-label "Plus tard !" \
                --ok-label     "OK"

	ans=$?
    if [ "${ans}" -eq 0 ]
    then
	    mintupdate
    fi
fi

exit 0
