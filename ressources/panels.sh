#!/bin/bash
#
# creation des tableaux de bord perso
#
# Param 1 :
#  1 paneau base seul
#  2 deux paneaux
#  3 paneau launcher seul
#
# Param 2 :
#  0, true -> cree les raccourcis principaux dans la barre des taches
#  autre chose -> sans raccourcis

cfg=1

if [ $# -ge 1 ] ; then cfg=$1  ; fi

# check input
if ! `echo $cfg | grep -qe "^\(-\|\)[0-9]*"`
then
    cfg=1
elif [[ $cfg -lt 1 ]] || [[ $cfg -gt 3 ]]
then
    echo "[ ERROR ] parametres valides : 1 2 3"
    exit
fi

panelBaseShortcuts="false"

if [ $# -ge 2 ] ; then tmppanelBaseShortcuts=$2 ; fi
case $tmppanelBaseShortcuts in 
    0) panelBaseShortcuts="true" ;;
    *) panelBaseShortcuts="false" ;;
esac

function plugin(){
	pluginName=$1
	PluginNumber=$((PluginNumber+1))
	xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}       -n -t string -s    $pluginName
}

function launcher(){
	app=$1
    echo "[ INFO ] tentative de creation du launcher ${app}.desktop"
	if [ -f /usr/share/applications/${app}.desktop ]
	then
		PluginNumber=$((PluginNumber+1))
		xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber} -n -t string -s "launcher"
		xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/arrow-position -n -t int -s 0   # 0 = default / 1 = north / 2 = west / 3 = east / 4 = south / 5 = inside
		xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/disable-tooltips -n -t bool -s false
		oldLauncherName=$LauncherName
		LauncherName="$(date +%s).desktop"
		if [ "$LauncherName" == "$oldLauncherName" ]
		then
			sleep 1
			LauncherName="$(date +%s).desktop"
		fi
		mkdir ~/.config/xfce4/panel/launcher-${PluginNumber}/
		cp -iv /usr/share/applications/${app}.desktop ~/.config/xfce4/panel/launcher-${PluginNumber}/${LauncherName}
		xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/items -n -a -t string -s "${LauncherName}"
		xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/move-first -n -t bool -s false
		xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-label -n -t bool -s false
	else
		echo "warning /usr/share/applications/${app}.desktop n'existe pas"
	fi
}

function separator(){
	# Separateur extend
	PluginNumber=$((PluginNumber+1))
	xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber} -n -t string -s "separator"
	xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/expand -n -t bool -s $1
	xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/style -n -t int -s 0   # 0 = transparent / 1 = separator / 2 = handle / 3 = dots
}

function panelBase(){
#
# Menu demarrer ( whisker )
#
PluginNumber=${PluginOffset}
PluginNumber=$((PluginNumber+1))
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber} -n -t string -s "whiskermenu"
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/button-icon -n -t string -s "distributor-logo"
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/button-title -n -t string -s "Applications"
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/custom-menu -n -t bool -s false
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/custom-menu-file -n -t string -s ""
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-button-title -n -t bool -s false
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-generic-names -n -t bool -s false
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-menu-icons -n -t bool -s true
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-tooltips -n -t bool -s true

if [ $panelBaseShortcuts == "true" ]
then

# Separateur
separator false

# Raccourcis home
launcher exo-file-manager
sed -e 's/Icon=.*/Icon=user-home/' -i ~/.config/xfce4/panel/launcher-${PluginNumber}/${LauncherName}

# Corbeille
plugin "thunar-tpa"

# Raccourcis
PluginNumber=$((PluginNumber+1))
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber} -n -t string -s "places"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/button-label -n -t string -s "Raccourcis"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/mount-open-volumes -n -t bool -s false
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/search-cmd -n -t string -s "catfish"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-bookmarks -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-button-type -n -t int -s 0 # 0 = icon / 1 = label / 2 = icon & label
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-icons -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-recent -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-recent-clear -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-recent-number -n -t int -s 5
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-volumes -n -t bool -s true


launcher xfce-settings-manager # Parametres
separator false

#
# Lanceurs d'applications
#
launcher firefox
launcher thunderbird
launcher libreoffice-writer
launcher libreoffice-calc

fi

# Tasklist
plugin "tasklist"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/middle-click -n -t int -s  1 # 1 = fermer fenetre
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/style -n -t int -s 0   # 0 = transparent / 1 = separator / 2 = handle / 3 = dots
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-wineframes -n -t bool -s true   # afficher le cadre des fenetres
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/grouping -n -t int -s 1   # regrouper les fenêtres de meme type
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/flat-buttons -n -t bool -s true   # boutons sans cadre true

separator true

# Zone de notifications
plugin "systray"     
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-frame -n -t bool -s false # cacher le cadre autour du plugin

plugin "indicator"   # Greffon indicateur
plugin "clock"       # Horloge
plugin "pager"       # Liste des bureaux

# Bouton de fermeture
PluginNumber=$((PluginNumber+1))
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}       -n -t string -s    "actions"

# bouton
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/items -n  \
-t string        -t string        -t string     -t string   -t string     -t string     -t string    -t string    -t string     -t string   -t string \
-s +lock-screen  -s -switch-user  -s -separator  -s -suspend  -s -hibernate  -s -separator  -s -shutdown  -s -restart  -s -separator  -s +logout  -s -logout-dialog
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/appearance -n -t int -s 0

# menu
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/items -n  \
#-t string        -t string        -t string     -t string   -t string     -t string     -t string    -t string    -t string     -t string   -t string \
#-s +lock-screen  -s +switch-user   -s +separator  -s +suspend  -s +hibernate  -s +separator  -s +shutdown  -s +restart  -s separator  -s -logout  -s +logout-dialog
#xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/appearance -n -t int -s 1

# Somme de tout le monde
PluginTotalNumber=${PluginNumber}
PanelNumber=$((PanelNumber+1))

xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/autohide -n -t bool -s false
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-alpha -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-image -n -t string -s ""
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-style -n -t int -s 0   # 0 = default (theme) / 1 = custom background color / 2 = image
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/disable-struts -n -t bool -s false
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/enter-opacity -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/leave-opacity -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/length -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/length-adjust -n -t bool -s true
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/mode -n -t int -s 0   # 0 = horizontal / 1 = vertical / 2 = deskbar
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/nrows -n -t int -s 1
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/plugin-ids -n -a $(for i in $(seq $((PluginOffset+1)) $((PluginTotalNumber))); do echo "-t int -s $i"; done | tr '\n' ' ')
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/position -n -t string -s "p=6;x=0;y=0"   # "p5;x=?;y=?" = use x & y coordinates / "p6;x=0;y=0" = screen top / "p7;x=0;y=0" = screen middle / "p8;x=0;y=0" = screen bottom
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/position-locked -n -t bool -s true
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/size -n -t int -s 28
}

function panelLauncher(){
#
# Creation du second paneau
#
PluginOffset=$PluginTotalNumber
echo "PluginOffset $PluginOffset"

separator true
# Raccourcis home
launcher exo-file-manager
sed -e 's/Icon=.*/Icon=user-home/' -i ~/.config/xfce4/panel/launcher-${PluginNumber}/${LauncherName}

# Corbeille
plugin "thunar-tpa"

# Raccourcis
plugin "places"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/button-label -n -t string -s "Raccourcis"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/mount-open-volumes -n -t bool -s false
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/search-cmd -n -t string -s "catfish"
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-bookmarks -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-button-type -n -t int -s 0 # 0 = icon / 1 = label / 2 = icon & label
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-icons -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-recent -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-recent-clear -n -t bool -s true
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-recent-number -n -t int -s 5
xfconf-query -c xfce4-panel -p /plugins/plugin-${PluginNumber}/show-volumes -n -t bool -s true

# Parametres
launcher xfce-settings-manager

separator false
launcher firefox
launcher thunderbird
launcher libreoffice-startcenter
launcher pdfmod
separator false
launcher gimp
launcher vlc
launcher xfburn
launcher brasero
separator true

PluginTotalNumber=${PluginNumber}
PanelNumber=$((PanelNumber+1))

xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/autohide -n -t bool -s false
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-alpha -n -t int -s 0
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-image -n -t string -s ""
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/background-style -n -t int -s 0   # 0 = default (theme) / 1 = custom background color / 2 = image
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/disable-struts -n -t bool -s false
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/enter-opacity -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/leave-opacity -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/length -n -t int -s 100
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/length-adjust -n -t bool -s true
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/mode -n -t int -s 0   # 0 = horizontal / 1 = vertical / 2 = deskbar
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/nrows -n -t int -s 1
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/plugin-ids -n -a $(for i in $(seq $((PluginOffset+1)) $((PluginTotalNumber))); do echo "-t int -s $i"; done | tr '\n' ' ')
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/position -n -t string -s "p=12;x=0;y=0"   # "p5;x=?;y=?" = use x & y coordinates / "p6;x=0;y=0" = screen top / "p7;x=0;y=0" = screen middle / "p8;x=0;y=0" = screen bottom
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/position-locked -n -t bool -s true
xfconf-query -c xfce4-panel -p /panels/panel-${PanelNumber}/size -n -t int -s 54
}

#
# reset factory
#
echo '[ INFO ] reinitialisation des tableaux de bord aux paramètres initiaux'
xfconf-query -c xfce4-panel -p /panels  -r -R
xfconf-query -c xfce4-panel -p /plugins -r -R
# Si pas de xfce4-panel detecte xfce4-panel --quit affiche une fenetre comme quoi il ne peut rien 
# tuer… en le lancant en tache de fond et en le tuant eventuellement on contourne le probleme
xfce4-panel --quit & 
sleep 1
pkill xfce4-panel
#
pkill xfconfd
rm -rf ~/.config/xfce4/panel/
rm -rf ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
xfce4-panel &
sleep 3       # wait a little in order restart is donne properly
pkill migrate # kill the prompt asking if empty panel or default conf 
              # if clicked before, no matters, but if clic after our config begin to run, it's a mess

#
# Creation configuration personnalisee
#
xfconf-query -c xfce4-panel -p /plugins -r -R   # Remove completely the panel configuration

rm -rfv ~/.config/xfce4/panel/launcher*
rm -v ~/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml

mkdir -p  ~/.config/xfce4/panel/ ~/.config/xfce4/xfconf/xfce-perchannel-xml/

PanelOffset=-1
PluginOffset=0

PanelNumber=${PanelOffset}

LauncherName=0

# active wanted panels
case $cfg in 
    1)
        echo '[ INFO ] panneau basique'
        panelBase
        xfconf-query -c xfce4-panel -p /panels -n -a -t int -s 0
    ;;
    2)
        echo '[ INFO ] Barre de lanceurs'
        panelLauncher
        xfconf-query -c xfce4-panel -p /panels -n -a -t int -s 0
    ;;        
    3)
        echo '[ INFO ] Panneau basique + barre de launceurs'
        panelBase
        panelLauncher
        xfconf-query -c xfce4-panel -p /panels -n -a -t int -t int -s 0 -s 1
    ;;
esac


# redemarrage du panel
xfce4-panel -r
