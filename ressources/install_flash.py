#!/usr/bin/env python3
import argparse
import os
import platform

def Arch() :
    if   re.match( '32bit', platform.architecture()[0] ) :
        return 32
    elif re.match( '64bit', platform.architecture()[0] ) :
        return 64
    else :
        return False

if __name__ == '__main__' :
    euid=os.geteuid()
    if euid != 0 :
        print('[ ERROR ] relancez ce script avec les droits root !')
        exit()  
    
    parser = argparse.ArgumentParser(description='Desclicks Post Install Xubuntu')
    parser.add_argument(       '--install'                , help='Installer le plugin flash' , required=False, type=str      , choices=['none', 'adobe', 'gnu', 'pipelight', 'shumway'] )
    args   = parser.parse_args()
    
    # Remove all flash things related
    os.system(''' aptitude remove --purge -y flashplugin-installer gnash lightspark browser-plugin-lightspark pipelight-multi ''')
    
    # Install only what is wanted
    try :
        if args.install == 'adobe' :
            # Adobe
            os.system('''aptitude install -y flashplugin-installer''')
        
        elif args.install == 'gnu' :
            # Gnash + Lightspark
            os.system('''aptitude install -y gnash gstreamer0.10-ffmpeg gstreamer0.10-plugins-bad lightspark browser-plugin-lightspark ''')
        
        elif args.install == 'shumway' :
            # Shumway
            os.system('''wget http://mozilla.github.io/shumway/extension/firefox/shumway.xpi''')
            os.system('''unzip shumway.xpi -d {ec8030f7-c20a-464f-9b0e-13a3a9e97384}''')
            os.system('''cp -rf  {ec8030f7-c20a-464f-9b0e-13a3a9e97384} /usr/lib/firefox-addons/extensions''')
            
        # Pipelight
        elif args.install == 'pipelight' :
            os.system('''add-apt-repository ppa:pipelight/stable -y ''')
            os.system('''aptitude update''')
            os.system('''aptitude install --with-recommends -y pipelight-multi''')
            os.system('''echo Y | pipelight-plugin --enable flash''')
    except :
        print('''[ WARNING ] echec de l'installation de flash : ''' + args.install )
