#!/usr/bin/env python3
import argparse
import os
import platform
import re
import subprocess

def Arch() :
    if   re.match( '32bit', platform.architecture()[0] ) :
        return 32
    elif re.match( '64bit', platform.architecture()[0] ) :
        return 64
    else :
        return False

if __name__ == '__main__' :
    euid=os.geteuid()
    if euid != 0 :
        print('[ ERROR ] relancez ce script avec les droits root !')
        exit()  
    
    parser = argparse.ArgumentParser(description='Desclicks Post Install Xubuntu')
    parser.add_argument( '--install', help='Installation et configuration de certains paquets non libres'             ,
                        nargs='*', required=False, default=list(), choices=['acroread', 'acroread11', 'googleearth', 'skype', 'acroread', 'teamviewer', 'teamviewer_qs'] )
    args   = parser.parse_args()
    #
    # Activate partner repositories
    #
    if args.deb_partner :
        distcodename = list(platform.linux_distribution())[2]

        if distcodename :
            if args.distrib == "mint" :
                if   distcodename == "rosa" :
                    distcodename = "trusty"
                elif distcodename == "sarah" :
                    distcodename = "xenial"
                
                os.system( '''sed -e 's&^# \(deb http://archive\.canonical\.com/ubuntu ''' + distcodename + ''' partner\)&\\1&' -i /etc/apt/sources.list.d/official-package-repositories.list''' )
            else :
                os.system( '''sed -e 's&^# \(deb http://archive\.canonical\.com/ubuntu ''' + distcodename + ''' partner\)&\\1&' -i /etc/apt/sources.list''' )
        else :
            print( '''[ ERROR ] echec le l'activation des depots partenaires ''')
            exit(1)
            
    os.system( '''aptitude update''')
    
    p=subprocess.Popen('logname', stdout=subprocess.PIPE)
    logname=p.communicate()[0].decode("utf-8").strip()
    #
    # Google earth
    #
    if 'googleearth' in args.install :
        try :
            os.system( '''./install_googleearth.sh''')
        except :
            print('''[ ERROR ] Google earth non installe''')
    #
    # Skype
    #
    #fix 64-bit 
    ##sudo apt-get install gtk2-engines-murrine:i386 sudo apt-get install gtk2-engines-pixbuf:i386

    #fix indicator
    ##sudo apt-get install sni-qt:i386
    if 'skype' in args.install :
        try :
            os.system('''aptitude install -y skype gtk2-engines-murrine gtk2-engines-pixbuf sni-qt''')
            print( Arch() )
            if Arch() == 64 : 
                print('''[ FIX ] skype for 64''')
                os.system('''aptitude install -y gtk2-engines-murrine:i386 gtk2-engines-pixbuf:i386 sni-qt:i386''')
        except :
             print('''[ ERROR ] skype non installe''')

    #
    # Acrobat Reader deb
    #
    if 'acroread' in args.install :
        try :
            os.system('''aptitude install -y libgtk2.0-0:i386 libnss3-1d:i386 libnspr4-0d:i386 lib32nss-mdns libxml2:i386 libxslt1.1:i386 libstdc++6:i386''')
            os.system('''wget ftp://ftp.adobe.com/pub/adobe/reader/unix/9.x/9.5.5/enu/AdbeRdr9.5.5-1_i386linux_enu.deb -O acroread.deb''')
            os.system('''aptitude install libgtk2.0-0:i386 libnss3-1d:i386 libnspr4-0d:i386 libnss-mdns libxml2:i386 libxslt1.1:i386 libstdc++6:i386''')
            os.system('''dpkg -i --force-depends acroread.deb''')
            os.system('''apt-get -y -f install''')
        except :
            print('''[ ERROR ] Acrobat reader 9 non installe''')
            
    if 'acroread11' in args.install :
        try :
            os.system('''./install_acroread11.sh''')
        except :
            print('''[ ERROR ] Acrobat reader 11 non installe''')

    #
    # Teamviewer
    #
    if 'teamviewer' in args.install :
        try :
            os.system('''wget -O teamviewer_linux.deb  download.teamviewer.com/download/teamviewer_i386.deb''')
            os.system('''dpkg -i teamviewer_linux.deb''')
            os.system('''apt-get install -f -y''')
        except :
            print('''[ ERROR ] Teamviewer non installe''')

    if 'teamviewer_qs' in args.install :
        try :
            os.system('''./install_teamviewer_qs.sh''')
            try :
                os.system('''usermod -aG teamviewer ''' + logname )
            except :
                print('''[ ERROR ] could not add user to teamviewer group''')
                        
        except :
            print('''[ ERROR ] Teamviewer 'Aide rapide' non installe''')
