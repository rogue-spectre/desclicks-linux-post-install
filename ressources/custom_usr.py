#!/usr/bin/env python3
import argparse
import os

if __name__ == '__main__' :
    euid=os.geteuid()
    if euid == 0 :
        print('[ ERROR ] lancez ce script avec un utilisateur normal !')
        exit()

    parser = argparse.ArgumentParser(description='Desclicks Post Install Xubuntu')
    parser.add_argument(       '--desktop'              , help='Type de bureau utilise xfce, unity' , required=False, default='xfce', choices=['none', 'xfce','unity'] )
    parser.add_argument(       '--prefered-apps'          , help="Choix d'applications par defaut"    , required=False, nargs='*', default=list(), choices=['vlc', 'gthumb','brasero'] )
   
    args = parser.parse_args()

    if args.desktop == 'xfce' :
    
        # force refresh
        if os.path.exists('/usr/share/icons/Faenza-Darkest') :
            os.system( 'xfconf-query --channel xsettings     --property "/Net/IconThemeName" --set ""')
            
        os.system( 'xfconf-query --channel xfwm4         --property "/general/theme" -s  ""')
        os.system( 'xfconf-query --channel xsettings     --property "/Net/ThemeName" -s  ""')
        os.system( 'xfconf-query --channel xfce4-notifyd --property "/theme"         -s  ""')
        
        if os.path.exists('/usr/share/icons/Faenza-Darkest') :
            os.system( 'xfconf-query --channel xsettings     --property "/Net/IconThemeName" --set Faenza-Darkest')
        
        os.system( 'xfconf-query --channel xfwm4         --property "/general/theme" -s  Numix')
        os.system( 'xfconf-query --channel xsettings     --property "/Net/ThemeName" -s  Greybird')
        os.system( 'xfconf-query --channel xfce4-notifyd --property "/theme"         -s  Numix')
        
        # desktop show icons
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-home"               -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-filesystem"         -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-trash"              -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-unknown-removable"  -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-removable"          -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-network-removable"  -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-device-removable"   -s false')
        
        #
        # Shortcuts
        #
        # super opens menu
        os.system( 'xfconf-query --channel  xfce4-keyboard-shortcuts --property "/commands/custom/Super_R" --create --type string -s xfce4-popup-whiskermenu')
        # ctrl+alt+suppr opens task manager
        os.system('xfconf-query --channel  xfce4-keyboard-shortcuts --property "/commands/custom/<Primary><Alt>Delete" --create --type string -s xfce4-taskmanager')

        # desactivation de la restauration de l'etat du verouillage numerique
        os.system('xfconf-query --channel keyboards --property "/Default/RestoreNumlock" --create --type bool -s false')    
        
        #
        # Wallpaper
        #
        wallpaper='/usr/share/xfce4/backdrops/wallpaper.png'
        if os.path.exists( wallpaper ) :
            os.system('./change_xfce_wallpaper.sh ' + wallpaper )
            #os.system('xfconf-query --channel xfce4-desktop --property "/backdrop/screen0/monitor0/image-path" -n -t string -s ""')
            #os.system('xfconf-query --channel xfce4-desktop --property "/backdrop/screen0/monitor1/image-path" -n -t string -s ""')
            #os.system('xfconf-query --channel xfce4-desktop --property "/backdrop/screen0/monitor0/image-path" -n -t string -s "/usr/share/xfce4/backdrops/wallpaper.png"')
            #os.system('xfconf-query --channel xfce4-desktop --property "/backdrop/screen0/monitor1/image-path" -n -t string -s "/usr/share/xfce4/backdrops/wallpaper.png"')
            #os.system('xfconf-query --channel xfce4-desktop --property "/backdrop/screen0/monitorVirtual1/workspace0/last-image" -n -t string -s /usr/share/xfce4/backdrops/wallpaper.png')
            #os.system('xfconf-query --channel xfce4-desktop --property "/backdrop/screen0/monitorVirtual1/workspace1/last-image" -n -t string -s /usr/share/xfce4/backdrops/wallpaper.png')
        
        if 'vlc' in args.prefered_apps :
            os.system('''xfconf-query  -c thunar-volman -p '/autoplay-audio-cds/command' -n -t string -s 'vlc -q cdda://%d' ''')
            os.system('''xfconf-query  -c thunar-volman -p '/autoplay-video-cds/command' -n -t string -s 'vlc -q --fullscreen dvd://%d' ''')
            
        if 'brasero' in args.prefered_apps :
            os.system('''xfconf-query  -c thunar-volman -p '/autoburn/audio-cd-command' -n -t string -s 'brasero -a' ''')
            os.system('''xfconf-query  -c thunar-volman -p '/autoburn/data-cd-command'  -n -t string -s 'brasero -d' ''')
            
    elif args.desktop == 'unity' :
        if os.path.exists('/usr/share/icons/Faenza-Darkest') :
            os.system('''gsettings set org.gnome.desktop.interface icon-theme 'Faenza-Darkest' ''')
            
        os.system('''gsettings set com.canonical.unity.webapps preauthorized-domains  [] ''')
        
