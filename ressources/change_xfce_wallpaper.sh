#!/bin/bash
 
if [ ! $# == 1 ]
then
    echo "[ ERROR ] invalid number of arguments"
    echo "          full path of wallpaper needed"
    exit 1
fi
 
wallpaper=$1 #donner le chemin exact
 
if [ ! -f $wallpaper ]
then
   echo "[ ERROR ] file not found"
   exit 1
fi
 
# Recuperer la liste des écrans connecté et actifs
connectedOutputs=$(xrandr | grep " connected" | sed -e "s/\([A-Z0-9]\+\) connected.*/\1/")
activeOutput=$(xrandr | grep -e " connected [^(]" | sed -e "s/\([A-Z0-9]\+\) connected.*/\1/") 
connected=$(echo $connectedOutputs | wc -w)
 
# Appliquer le fond d'écran
 
for i in $(xfconf-query -c xfce4-desktop -p /backdrop -l|egrep -e "screen.*/monitor.*image-path$" -e "screen.*/monitor.*/last-image$"); do
 
    xfconf-query -c xfce4-desktop -p $i -n -t string -s ""
    xfconf-query -c xfce4-desktop -p $i -n -t string -s $wallpaper
    xfconf-query -c xfce4-desktop -p $i -n -s ""
    xfconf-query -c xfce4-desktop -p $i -n -s $wallpaper
 
done
 
exit 0
