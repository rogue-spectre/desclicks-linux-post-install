#!/bin/bash

#_______________________________________________________________________
#
# Configuration des comptes en ligne
#_______________________________________________________________________

#while [ $isOnlineAccountInit == "false" ]
#while [ ! -f isOnlineAccountInit ]
#do
#    echo '[ INFO ] wait initialisation about unity control-center'
#    sleep 1
#done

onlineAccountInit | zenity --progress --pulsate --auto-close --text "Recherche des informations sur les comptes en ligne"

onlineAccountPluginsToRemove=$( cat onlineAccountpluginsRawList | grep    ^i |sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2)
onlineAccountPluginsList=$(     cat onlineAccountpluginsRawList |             sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
onlineAccountPluginsInstalled=$(cat onlineAccountpluginsRawList | grep    ^i |sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
onlineAccountPluginsAvailable=$(cat onlineAccountpluginsRawList | grep -v ^i |sed -e 's/\(.\).../\1   /' -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)

#if `aptitude search unity-control-center-signon | grep -q ^i`
#if [ "$isOnlineAccountCenterInstalled" == "true" ]
if [ -f "OnlineAccountCenterInstalled" ]
then
    onlineAccountcenterStatus='deja installe'
    onlineAccountcenterProp='FALSE           Supprimer         remove'
else
    onlineAccountcenterStatus='non installe'
    onlineAccountcenterProp='FALSE          Installer         install'
fi

onlineAccountcenter=$(
zenity --list \
--height=400 \
--width=400 \
--title "Installation/Suppression du centre de comptes en ligne" \
--text "
Le centre de contrôle des comptes en ligne est nécessaire pour la 
publication de contenus en lignes. 

<span color=\"grey\">la version de shotwell installable ici fonctionne 
avec picasaweb sans nécessiter l'installation de centre de contrôle</span>

Tous les plugins de comptes seront supprimés si le centre de contrôle n'est pas installé

État du paquet : <span color=\"red\">$onlineAccountcenterStatus</span>" \
--radiolist \
--hide-column=3 \
--column "Pick" --column "options"         --column "id" \
            "TRUE"            "Garder en l'état"   "keep"    \
            $onlineAccountcenterProp \
            --print-column=3 \
            );
            
exitZenity=$?
if  [ $exitZenity != 0 ] 
then
    zenity --warning --text "Annulation lors de la configuration de centre des comptes en ligne"
exit 1
fi

case $onlineAccountcenter in
"install")
__install__=" $__install__ unity-control-center-signon"
;;
"remove")
__remove__=" $__remove__ unity-control-center-signon"
;;
esac

#if [ $onlineAccountcenter = "keep" ] && `aptitude search unity-control-center-signon | grep -q ^i`  || [ $onlineAccountcenter = "install" ]
#if [ $onlineAccountcenter = "keep" ] && [ $isOnlineAccountCenterInstalled == "true" ] || [ $onlineAccountcenter == "install" ]
if [ $onlineAccountcenter = "keep" ] && [ -f "OnlineAccountCenterInstalled" ] || [ $onlineAccountcenter == "install" ]
then
    #onlineAccountsPluginsList=$(aptitude search ^account-plugin |             sed -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
    
    #onlineAccountsPluginsInstalled=$(aptitude search ^account-plugin | grep    ^i |sed -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)
    #onlineAccountsPluginsAvailable=$(aptitude search ^account-plugin | grep -v ^i |sed -e 's/ \s*/ /' | cut -d ' ' -f 2 | cut -d '-' -f 3)

    #if `aptitude search ^account-plugin | grep -q ^i `
    if [ ! -z "$onlineAccountPluginsInstalled" ]
    then
        onlineAccountpluginsToRemove=$(
        for i in $onlineAccountPluginsInstalled
        do
            echo "FALSE"
            echo "$i"
        done | zenity --list --checklist --title="Supprimer des plugins" --text="Supprimer des plugins" --column="Pick" --column="plugin" --separator=" account-plugin-")
    fi
    
    exitZenity=$?
    
    if  [ $exitZenity != 0 ] 
    then
        zenity --warning --text "Annulation lors de la suppression des plugins"
    exit 1
    fi
    
    if [ ! -z "$onlineAccountpluginsToRemove" ]
    then
        __remove__="$__remove__ account-plugin-$onlineAccountpluginsToRemove"
    fi
    
    #if `aptitude search ^account-plugin | grep -v -q ^i `
    if [ ! -z "$onlineAccountPluginsAvailable" ]
    then 
        onlineAccountpluginsToInstall=$(
        for i in $onlineAccountPluginsAvailable
        do
            echo "FALSE"
            echo "$i"
        done | zenity --list --checklist --title="Installer des plugins" --text="Installer des plugins" --column="Pick" --column="plugin" )
    fi
    
    exitZenity=$?
    
    if  [ $exitZenity != 0 ] 
    then
        zenity --warning --text "Annulation lors de l'installation des nouveaux plugins"
    exit 1
    fi
    
    if [ ! -z "$onlineAccountpluginsToInstall" ]
    then
        __install__="$__install__ account-plugin-$onlineAccountpluginsToInstall"
    fi
fi
