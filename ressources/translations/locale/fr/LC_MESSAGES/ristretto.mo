��    �      �  �   �      �     �  L   �     �  	   �     �     	          )     :     G     T     [     t     �     �  9   �  7   �  5   &  2   \     �     �  	   �     �     �     �     �     �     �          
  �        �     �     �     �                .     J  
   R     ]  #   e     �     �     �  	   �     �  
   �     �     �       
             "  "   *     M     \     b     o     �     �     �     �     �  	   �     �  
   �     �     �     	       
   &     1  0   L  +   }     �  
   �     �     �  (   �              
   .     9     E     X     s     �     �     �     �     �     �     �     �       >        Y     f     t     �  	   �     �     �     �     �     �     �  	          Q   "     t     �  	   �     �     �     �     �     �  	   �     �     �       L   .  �   {  P   0     �     �  
   �     �     �     �     �     �  %   �       
     
   &  W   1     �     �  	   �     �  	   �     �     �     �     �     �     �     �            	   !     +     3     9     ?     H     T     X     ^     d     v  
   �     �     �     �  	   �     �     �     �     �  	   �     �     �                         #     0     A  �  T     (  g   :     �  	   �     �     �     �     �     �     
         ,   "   -   O   "   }   '   �   W   �   Z    !  @   {!  E   �!     "     "     ("     5"     <"     K"  @   S"  	   �"  !   �"     �"     �"  �   �"  
   �#      �#  %   �#  0   �#  1   
$  +   <$     h$     �$     �$  	   �$  0   �$  .   �$     %     $%     9%     G%     L%  
   Q%     \%     l%     |%     �%  	   �%  4   �%     �%     �%     �%  &   &     .&     5&     N&  
   T&     _&     |&     �&     �&     �&     �&  	   �&     �&     '      '  3   @'  >   t'  !   �'     �'     �'     �'  3   �'  %   *(     P(     a(     r(     ~(  ,   �(     �(     �(  5   �(     ))     <)     E)     W)     k)  	   �)     �)  M   �)     �)     *     0*  "   >*     a*     w*     �*  "   �*     �*  .   �*     +     )+  !   ;+  e   ]+  $   �+  $   �+  	   ,     ,     ,     *,     A,  
   `,     k,     s,     {,  '   �,  Q   �,  �   -  F   �-     .  $   5.  
   Z.  	   e.     o.     �.     �.     �.  -   �.     �.     �.     �.  g   /      n/     �/     �/     �/     �/  
   �/     �/     �/     �/     0  
   0     0  %   !0     G0     O0  
   f0  	   q0     {0  	   �0     �0  	   �0     �0  
   �0     �0     �0     �0  
   �0     �0  
   �0  	   
1     1     &1     71     @1  	   U1     _1     y1     �1     �1  
   �1     �1     �1     �1  -   �1            m      R             h         j   �   =       �   ]   �   U   A   (   �          �       B   -       Y   +       '   u       ,      �   e   	   g   N   �   4   �       �       \   G   �   �   L   n   7       �       �   K   <   �   w   t       �          S   �      �   C           V          r   2   y   #   Z   o   "   f   �       T   ~   E       �   x       c   �   8          9   �   �   0                                             �   p   �      P   �   O   �       q   {       �   �          �   5   ;   �       &   �       @           k   $           �   �   �   i   �   �   �                 1       b   �   �   X   �      3   >              *   }   6   |   J   �   I       v   _      ?   D                  �   �       /   �       �   `   )         �           �   s   �       �          �   !       [              M       d   �   l       
   ^   �           z   F   :          W              �      a   .   �   �       �   �               Q   H   %    %s - Properties %s: %s

Try %s --help to see a full list of
available command line options.
 .jp(e)g <b>%s</b> <b>Accessed:</b> <b>Date taken:</b> <b>Kind:</b> <b>Modified:</b> <b>Name:</b> <b>Size:</b> Active Active item border color Active item fill color Active item index Active item text color An error occurred when deleting image '%s' from disk.

%s An error occurred when sending image '%s' to trash.

%s Are you sure you want to delete image '%s' from disk? Are you sure you want to send image '%s' to trash? Auto Background color Behaviour Bottom Brightness: Centered Choose 'set wallpaper' method Cleanup Clear private data Clock Close this image Configure which system is currently managing your desktop.
This setting determines the method <i>Ristretto</i> will use
to configure the desktop wallpaper. Control Could not open file Could not save file Cursor item border color Cursor item fill color Cursor item text color Delete this image from disk Desktop Developer: Display Display information about ristretto Display ristretto user manual Do _not show this message again Edit this image Edit with Empty Everything F_irst File column First image Fullscreen GNOME General Hide thumbnail bar when fullscreen Icon Bar Model Image Image Viewer Image Viewer Preferences Images Invert zoom direction Large Larger Last Four Hours Last Hour Last Two Hours Last image Leave Fullscreen Leave _Fullscreen Left Limit rendering quality Loading... Look at your images easily Maximize window on startup when opening an image Model column used to retrieve the file from Model for the icon bar Next image None Normal Open %s and other files of type %s with: Open With Other _Application... Open an image Open image Orientation Other Applications Override background color: Pause slideshow Play slideshow Press open to select an image Previous image Quality Quit Ristretto Recently used Recommended Applications Right Ristretto Image Viewer Ristretto is an image viewer for the Xfce desktop environment. Rotate _Left Rotate _Right Saturation: Save a copy of the image Save copy Scaled Scroll wheel Set as _Wallpaper... Set as wallpaper Show Fullscreen Clock Show Status _Bar Show Text Show _Thumbnail Bar Show an analog clock that displays the current time when the window is fullscreen Show file properties Show settings dialog Slideshow Small Smaller Start a slideshow Start in fullscreen mode Startup Stretched Style: Switch to fullscreen The orientation of the iconbar The thumbnail bar can be automatically hidden when the window is fullscreen. The thumbnailer-service can not be reached,
for this reason, the thumbnails can not be
created.

Install <b>Tumbler</b> or another <i>thumbnailing daemon</i>
to resolve this issue. The time period an individual image is displayed during a slideshow
(in seconds) Thumb_nail Size Thumbnail Bar _Position Thumbnails Tiled Time range to clear: Timeout Today Top Use as _default for this kind of file Version information Very Large Very Small With this option enabled, the maximum image-quality will be limited to the screen-size. Wrap around images Xfce Zoom _Fit Zoom _In Zoom _Out Zoom in Zoom out Zoom to 100% Zoom to fit window Zoomed _About _Back _Clear private data... _Close _Contents _Delete _Edit _File _Forward _Fullscreen _Go _Help _Last _Leave Fullscreen _Normal Size _Open with _Open... _Pause _Play _Position _Preferences... _Properties... _Quit _Recently used _Rotation _Save copy... _Show Toolbar _Size _Sorting _View _Zoom sort by date sort by filename translator-credits Project-Id-Version: Xfce Apps
Report-Msgid-Bugs-To: xfce-i18n@xfce.org
POT-Creation-Date: 2013-07-03 20:30+0200
PO-Revision-Date: 2014-08-27 12:17+0100
Last-Translator: Urien Desterres <urien.desterres@gmail.com>
Language-Team: French (http://www.transifex.com/projects/p/xfce-apps/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.5.4
 %s - Propriétés %s : %s

Essayez %s --help pour voir une liste complète
des options de ligne de commande disponibles.
 .jp(e)g <b>%s</b> <b>Accédé :</b> <b>Date de prise :</b> <b>Type :</b> <b>Modifié :</b> <b>Nom :</b> <b>Taille :</b> Activer Couleur de la bordure de l’élément actif Couleur de remplissage de l’élément actif Activer l’index de l’élément Couleur du texte de l’élément actif Une erreur s’est produite lors de la suppression de l’image « %s » du disque.

%s Une erreur s’est produite lors de l’envoi de l’image « %s » dans la corbeille.

%s Voulez-vous vraiment supprimer l’image « %s » du disque ? Voulez-vous vraiment mettre l’image « %s » dans la corbeille ? Automatique Couleur d’arrière-plan Comportement En bas Luminosité :  Centré Choisir la méthode pour « Définir comme fond d’écran... » Nettoyage Effacer les données personnelles Horloge Fermer cette image Indiquez quel système gère actuellement votre bureau.
Ce réglage permettra à <i>Ristretto</i> de déterminer la
méthode adéquate pour en modifier le fond d’écran. Contrôles Impossible d’ouvrir le fichier Impossible d’enregistrer le fichier Couleur de la bordure de l’élément surligné Couleur de remplissage de l’élément surligné Couleur du texte de l’élément surligné Supprimer cette image du disque Bureau Programmeur : Affichage Afficher les informations à propos de Ristretto Afficher le guide d’utilisation de Ristretto _Ne plus afficher ce message Modifier cette image Modifier avec Vide Tout Pre_mière Colonne fichier Première image Plein écran GNOME Général Masquer la barre des miniatures en mode plein écran Modèle de barre d’icônes Image Visionneur d’images Préférences du visionneur d’images Images Inverser le sens du zoom Large Plus large Les quatre dernières heures La dernière heure Les deux dernières heures Dernière image Quitter le plein écran Quitter le _plein écran À gauche Limiter la qualité du rendu Chargement... Visionner vos images facilement Agrandir la fenêtre à l’ouverture d’une image Colonne du modèle utilisé pour récupérer le fichier depuis Modèle pour la barre d’icônes Image suivante Aucun Par défaut Ouvrir %s et les autres fichiers de type %s avec : Ouvrir avec une autre _application... Ouvrir une image Ouvrir une image Orientation Autres applications Remplacer la couleur d’arrière-plan par : Mettre en pause le diaporama Démarrer le diaporama Cliquez sur « Ouvrir » pour sélectionner une image Image précédente Qualité Quitter Ristretto Récemment utilisé Applications recommandées À droite Visionneur d’images Ristretto Ristretto est un visionneur d’images pour l’environnement de bureau Xfce. Rotation vers la _gauche Rotation vers la _droite Saturation :  Enregistrer une copie de l’image Enregistrer une copie Redimensionné Molette de la souris _Définir comme fond d’écran... Définir comme fond d’écran Afficher l’horloge lors du mode plein écran Afficher la barre d’ét_at Afficher le texte Afficher la barre des _miniatures Afficher une horloge analogique indiquant l’heure courante lorsque la fenêtre est en plein écran. Afficher les propriétés du fichier Afficher la fenêtre des paramètres Diaporama Petite Plus petite Démarrer un diaporama Démarrer en mode plein écran Démarrage Étiré Style : Basculer en plein écran L’orientation de la barre d’icônes La barre des miniatures peut être automatiquement masquée en mode plein écran. Impossible d’accéder au service des miniatures.
Celles-ci ne peuvent donc être créées.

Installez <b>Tumbler</b> ou un autre <i>démon de création
de miniatures</i> pour résoudre ce problème. Durée d’affichage d’une image lors d’un diaporama (en secondes) _Taille des miniatures P_osition de la barre des miniatures Miniatures Mosaïque Période à effacer :  Durée Aujourd’hui En haut Utiliser par _défaut pour ce type de fichier Informations sur la version Très large Très petite Avec cette option activée, la qualité maximale de l’image sera limitée à la taille de l’écran. Défilement en boucle des images Xfce Zoom a_justé Zoom _avant Zoom a_rrière Zoom avant Zoom arrière Taille réelle Zoom ajusté à la fenêtre Zoomé À _propos _Précédente _Effacer les données personnelles... _Fermer _Guide d’utilisation _Supprimer É_dition _Fichier _Suivante _Plein écran A_ller à Aid_e Der_nière _Quitter le plein écran _Taille réelle _Ouvrir avec _Ouvrir... _Pause _Démarrer _Position _Préférences... _Propriétés... _Quitter _Récemment utilisé _Rotation _Enregistrer une copie... Afficher la barre d’o_utils _Taille _Trier _Affichage _Zoom par date par nom de fichier L’équipe de traduction francophone de Xfce 