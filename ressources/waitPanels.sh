#!/bin/bash

rm -rf endWaitPanels
rm -rf beginWaitPanels

while [ ! -f beginWaitPanels ]
do
    sleep 1
done

./panels.sh $*
touch endWaitPanels

exit 0
