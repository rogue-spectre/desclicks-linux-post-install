#!/bin/bash

# accept eula
echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections

#aptitude install -y ttf-mscorefonts-installer libfreeimage3 lsb-core \
#libqtcore4 libcurl3:i386 libsm6:i386 libfontconfig1:i386 \
#libxt6:i386 libxrender1:i386 libxext6:i386  \
#libgl1-mesa-glx:i386 libgl1-mesa-dri:i386 

aptitude install -y googleearth-package

rm -rf tmpgoogleearth
mkdir tmpgoogleearth
cd tmpgoogleearth
rm -rf googleearth*.deb
make-googleearth-package --force
dpkg -i googleearth*.deb
apt-get install -f -y
cd ..
rm -rf tmpgoogleearth
