#!/usr/bin/env python3
import argparse
import os
import re
import platform
import subprocess
import time

def Arch() :
    if   re.match( '32bit', platform.architecture()[0] ) :
        return 32
    elif re.match( '64bit', platform.architecture()[0] ) :
        return 64
    else :
        return False


def killUnwanted() :
    ''' Tuer tous les processus genant le processus d'installation'''
    try :
        os.system('''killall -9 aptitude apt-get dpkg software-center gdebi synaptic''')
        os.system('''sleep 2''')
        os.system('''dpkg --configure -a ''')
    except :
        print('''[ ERROR ] lors de la phase d'elimination des processesus d'installation''')
    
    try :
        os.system('''killall -9 light-locker xscreensaver''')
    except :
        print('''[ ERROR ] Echec de l'extinction des gestionnaires  verrouillage d'ecran''')

def getVersion( packageName ) :
    try :
        os.system(''' aptitude versions ''' + packageName + ''' | grep ^i | grep -oe '[0-9]\.[0-9]*\.[0-9]*-[0-9]*' > tmp-version ''')
        with open('tmp-version', 'r') as f :
            version=f.readline()
            f.close()
        version=LooseVersion( version.strip() )
    except :
        version=None
    
    return version


if __name__ == '__main__' :
    euid=os.geteuid()
    if euid != 0 :
        print('[ ERROR ] relancez ce script avec les droits root !')
        exit()    

    parser = argparse.ArgumentParser(description='Desclicks Post Install Xubuntu')
    parser.add_argument(       '--install'              , help='Installation programmes custom'                                           , nargs='*',           required=False, default=list() )
    parser.add_argument(       '--remove'               , help='Deinstallation programmes custom'                                         , nargs='*',           required=False, default=list() )
    parser.add_argument(       '--noupdate'             , help='force no update'                                                          , action='store_true', required=False )   
    parser.add_argument(       '--rmmod'                , help='Blacklistage de modules'                                                  , nargs='*',           required=False, default=list(), choices=['floppy', 'mei_me'] )
    parser.add_argument(       '--apt'                  , help='Configurations pour apt'                                                  , action='store_true', required=False )    
    parser.add_argument(       '--skel'                 , help='Copie les fichiers de configuration de etc/skel dans /etc/skel'           , action='store_true', required=False )
    parser.add_argument(       '--pdf-hello'            , help='PDF de bienvenue copie sur le bureau'                                     , action='store_true', required=False )
    parser.add_argument(       '--numpad'               , help='Installer numlock + activation du pave au demarrage'                      , action='store_true', required=False )
    #parser.add_argument(       '--pol'                  , help='Installer playonlinux'                                                    , action='store_true', required=False )
    parser.add_argument(       '--flash'                , help='Installer le plugin flash'                                                                     , required=False, type=str      , choices=['none', 'adobe', 'gnu', 'pipelight', 'shumway'] )
    #parser.add_argument(       '--ttf-mscorefonts'      , help='Installer les polices microsoft'                                          , action='store_true', required=False )
    parser.add_argument(       '--dvd'                  , help='installer librairies de lecture des cd/dvd du commerce + config'          , action='store_true', required=False )
    parser.add_argument(       '--fixubuntu'            , help='Supprime les scopes non necessaires'                                      , action='store_true', required=False )    
    parser.add_argument(       '--no-session-save'      , help='supprimer l option de sauvegarde de session pour un prochain redemarrage' , action='store_true', required=False )
    parser.add_argument(       '--logo'                 , help='Logo custom pour le menu "demarrer"'                                                           , required=False, type=str )
    parser.add_argument(       '--panel'                , help='Tableau de bord ameliore'                                                 , action='store_true', required=False )
    parser.add_argument(       '--deb-partner'          , help='Activer les depots partenaires'                                           , action='store_true', required=False )
    parser.add_argument(       '--xpi'                  , help='Selection des XPI pour firefox'                                           , nargs='*',           required=False, default=list(), choices=['ALL', 'adb','httpsEverywhere', 'NoScript'])
    parser.add_argument(       '--desktop'              , help='Type de bureau utilise xfce, unity, mate'                                                      , required=False, default='none', choices=['none', 'xfce','unity', 'mate'] )
    parser.add_argument(       '--distrib'              , help='Nom de la distribution maitresse, ubuntu, mint'                           , required=False, default='none', choices=['none', 'ubuntu','mint'] )
    parser.add_argument(       '--use-mintupdate'       , help='Utiliser MintUpdate pour faire les mises à jour'                          , action='store_true', required=False )
    parser.add_argument(       '--lang'                 , help='Selection des langues a mettre a jour'                                    , nargs='*'          , required=False, type=str, default=list()  )
    parser.add_argument(       '--lightdm'              , help='Parametres du gestionnaire de connexion lightdm'                          , nargs='*'          , required=False, default=list(), choices=['noguest', 'numpad'] )
    parser.add_argument(       '--light-locker'         , help='Parametres pour le gestionnaire de verrouillage'                                               , required=False, type=str      , choices=['off',] )
    parser.add_argument(       '--libreoffice'          , help='Parametres pour libreoffice'                                              , nargs='*'          , required=False, default=list(), choices=['orthographe',] )
    #parser.add_argument(       '--online-account-center', help='Configuration des comptes en ligne'                                       , nargs='*'          , required=False, type=list )
    parser.add_argument(       '--selection'            , help='Selection de logiciels predefinis'                                        , nargs='*'          , required=False, default=list(), choices=['dev', 'utils', 'bureau', 'multimedia', 'infographie', 'education', 'tux', 'jeux', 'edu'] )
    parser.add_argument(       '--extra-non-free'       , help='Installation et configuration de certains paquets non libres'             , nargs='*'          , required=False, default=list(), choices=['acroread', 'acroread11', 'googleearth', 'skype', 'acroread', 'teamviewer', 'teamviewer_qs'] )
    parser.add_argument(       '--swap'                 , help='Diminuer la priorite du swap'                                             , type=int           , required=False                , choices=range(0, 61) )
    parser.add_argument(       '--patchs'               , help='Appliquer differentes corrections'                                        , nargs='*'          , required=False, default=list(), choices=['eject','users-admin'] )

    args = parser.parse_args()
    
    if 'skype' in args.extra_non_free : args.deb_partner=True
    
    #logname = subprocess.getoutput('logname') # recupere le nom de l'utilisateur qui a lance le script en admin
    p=subprocess.Popen('logname', stdout=subprocess.PIPE)
    logname=p.communicate()[0].decode("utf-8").strip()
    #
    # Path of some configuration files
    #    
    lightdm_xubuntu_conf = '/etc/lightdm/lightdm.conf.d/10-xubuntu.conf'
    lightdm_ubuntu_conf  = '/usr/share/lightdm/lightdm.conf.d/50-ubuntu.conf'

    urlLOextOrthographe  = 'http://www.languagetool.org/download/LanguageTool-2.5.oxt'
    
    # diacollecte 
    urlMOZextOrthographe = 'https://addons.mozilla.org/firefox/downloads/latest/354872/addon-354872-latest.xpi'
    
    #
    # check-language-support -l fr -l en
    #
    install = list()
    
    lang        = [ 'language-pack-fr',
                    'language-pack-gnome-fr'] 

    remove      = [ 'ubufox', 'xul-ext-ubufox' ]
                    
    searchpluginsBlacklist = [ 'amazon', 'bing', 'twitter', 'yahoo', 'eBay' ]
    #
    unity       = [ '-P ~n^unity-scope',
                   'unity-scope-home=',
                   'unity-scopes-master-default=',]

    utils       = [ 'gksu',
                    'p7zip-full unzip',
                    'gparted',
                    'gnome-disk-utility',
                    'baobab', ]
                
    if args.numpad or ( 'numpad' in args.lightdm ) :
        args.numpad = True
        args.lightdm.append('numpad')
        install.append('numlockx')

    bureau      = [ 'libreoffice libreoffice-gtk libreoffice-gtk3 libreoffice-style-sifr', 
                    'pdfmod',
                    'verbiste', 'verbiste-gnome',
                    'gnome-font-viewer' ]
                    
    bureau_nf   = [ 'ttf-mscorefonts-installer', ]

    multimedia  = [ 'vlc', 'sound-juicer' ]

    infographie = [ 'gimp', 'mypaint' ]
    
    net         = [ 'firefox', ]

    dvd         = [ 'libdvdread4', 'ubuntu-restricted-extras' ]
    #
    # extra selection
    #    
    tux          = [ 'tuxpaint', 'tuxmath', 'supertuxkart', 'tuxtype' ]
    
    jeux         = [ 'tanglet', 'gnome-mahjongg', 'gnome-chess', 'aisleriot' ] 
    
    education    = [ 'klavaro', ]
    
    edu          = [ 'ubuntu-edu-preschool', 'ubuntu-edu-primary', 'ubuntu-edu-secondary', 'ubuntu-edu-tertiary' ]


    if args.install :
        install.extend( args.install )
        
    if args.libreoffice :
        install.extend( ['libreoffice libreoffice-gtk libreoffice-gtk3 libreoffice-style-sifr', ] )
    
    if 'fr' in args.lang and ( 'orthographe' in args.libreoffice ) : 
        install.extend(['libreoffice-java-common', 'default-jre'])

        
    if 'ttf-mscorefonts-installer' in args.install \
        or 'playonlinux'           in args.install \
        or 'googleearth'           in args.extra_non_free :
        install.extend( bureau_nf )
        os.system( 'echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections') #accept eula for ttf-mscorefont
    
    if args.dvd                          : install.extend( dvd )
    
    if 'utils'         in args.selection : install.extend( utils )    
    if 'bureau'        in args.selection : install.extend( bureau )
    if 'multimedia'    in args.selection : install.extend( multimedia )
    if 'infographie'   in args.selection : install.extend( infographie )
    if 'tux'           in args.selection : install.extend( tux )
    if 'jeux'          in args.selection : install.extend( jeux )
    if 'education'     in args.selection : install.extend( education )
    if 'edu'           in args.selection : install.extend( edu )

    # force support anglais
    args.lang.append('en')
 
    # kill les installateurs
    killUnwanted()
    
    #
    # Activate partner repositories
    #
    if args.deb_partner :
        distcodename = list(platform.linux_distribution())[2]

        if distcodename :
            if args.distrib == "mint" :
                if   distcodename == "rosa" :
                    distcodename = "trusty"
                elif distcodename == "sarah" :
                    distcodename = "xenial"
                
                os.system( '''sed -e 's&^# \(deb http://archive\.canonical\.com/ubuntu ''' + distcodename + ''' partner\)&\\1&' -i /etc/apt/sources.list.d/official-package-repositories.list''' )
            else :
                os.system( '''sed -e 's&^# \(deb http://archive\.canonical\.com/ubuntu ''' + distcodename + ''' partner\)&\\1&' -i /etc/apt/sources.list''' )
        else :
            print( '''[ ERROR ] echec le l'activation des depots partenaires ''')
            
    if 'shotwell' in install :
        remove.append('shotwell')
        os.system('sudo add-apt-repository -y ppa:yorba/ppa')

    #
    # Forçage de l'ouverture de mint update sur mint
    #
    if args.distrib == "mint" and args.use_mintupdate :
        os.system( 'mint-update' )
    else :
        args.use_mintupdate = False
    
    #
    # update upgrade remove and install
    #
    if install or args.install or args.remove or args.selection:
        os.system( 'apt-get update' )
        os.system( 'apt-get install -y aptitude' )

        try :
            if args.remove : remove.extend( args.remove )
            #os.system( 'aptitude remove --purge ' + ' '.join(args.remove))
        except :
            print( '[ WARNING ] echec lors de la suppression de logiciels')
        
        os.system( 'aptitude remove --purge -y ' + ' '.join(remove))

        if not args.use_mintupdate :
            os.system( 'aptitude -y upgrade' )
            os.system( 'aptitude -y dist-upgrade' )
        
        os.system( 'aptitude install -y ' + ' '.join(lang))
        
        if install or args.install or args.selection: 
            os.system( 'aptitude -y install ' + ' '.join(install) ) 

    elif not args.noupdate :
        os.system( 'apt-get update' )
        os.system( 'apt-get install -y aptitude' )
        
        if not args.use_mintupdate :
            os.system( 'aptitude -y upgrade' )
            os.system( 'aptitude -y dist-upgrade' )        

    #
    # Extras non free
    #        
    if args.extra_non_free :
        os.system('''python3 install_extra_nonfree.py --install ''' + ' '.join(args.extra_non_free) )

    #
    # Flash
    #
    if args.flash :
        os.system('''python3 install_flash.py --install ''' + str(args.flash) )


    if args.skel :
        os.system( 'cp -rf etc/skel   /etc' )
        if os.path.exists('/home/' + logname ) :
            os.system( 'chown ' + logname + '.' + logname + ''' -R etc/skel
                        cp -rfp etc/skel/. /home/''' + logname )
                        
        if args.desktop == 'xfce' :
            try :
                os.system('cp -rf xfce/skel /etc')
                if os.path.exists('/home/' + logname ) :
                    os.system( 'chown ' + logname + '.' + logname + ''' -R xfce/skel
                                 cp -rfp xfce/skel/. /home/''' + logname )    
                
            except :
                print('[ ERROR ] lors de la copie des fichiers de configuration de xfce dans /etc/skel')
    #
    # Amelioration de quelques traductions
    #
    if 'fr' in args.lang :
        from distutils.version import LooseVersion 

        def updatelocale( packageName, limitVersion ) :
            try :
                #os.system(''' aptitude versions ''' + packageName + ''' | grep ^i | grep -oe '[0-9]\.[0-9]*\.[0-9]*-[0-9]*' > tmp-version ''')
                #with open('tmp-version', 'r') as f :
                #    version=f.readline()
                #    f.close()
                #version=LooseVersion( version.strip() )
                version = getVersion( packageName )
                limit   = LooseVersion( limitVersion )
                langpack=['gnome-calculator',]
                if version <= limit :
                    if packageName in langpack :
                        os.system(''' cp -rf translations/locale/fr/LC_MESSAGES/''' + packageName + '''.mo /usr/share/locale-langpack/fr/LC_MESSAGES/''' + packageName + '''.mo ''')
                    else :
                        os.system(''' cp -rf translations/locale/fr/LC_MESSAGES/''' + packageName + '''.mo /usr/share/locale/fr/LC_MESSAGES/''' + packageName + '''.mo ''')
            except :
                print(''' [ WARNING ] Echec de la maj de fichier de locale pour  '''+ packageName)
        
        for packageName, limitVersion in    ( 'lightdm-gtk-greeter', '1.8.4-0' ), \
                                            ( 'ristretto'          , '0.6.3-2' ), \
                                            ( 'mousepad'           , '0.3.0-2' ), \
                                            ( 'xfce4-screenshooter', '1.8.1-2' ), \
                                            ( 'gnome-calculator'   , '3.10.2-0') :
            updatelocale( packageName, limitVersion)

        # Light-locker-settings
        try :
            os.system(''' aptitude versions light-locker-settings | grep ^i | grep -oe '[1-9]\.[0-9]*\.[0-9]*-[0-9]*' > tmp-version ''')
            with open('tmp-version', 'r') as f :
                version=f.readline()
                f.close
            version=LooseVersion( version.strip() )
            limit  =LooseVersion( '1.2.1-0' )
            if version <= limit :
                os.system(''' cp -f translations/light-locker-settings.py /usr/share/light-locker-settings/light-locker-settings/light-locker-settings.py''' )
        except :
            print(''' [ WARNING ] Echec de la maj de langue pour light-locker-settings ''')
            
        # File manager desktop file
        try :
            os.system(''' cp -f translations/exo-file-manager.desktop       /usr/share/applications/''')
            os.system(''' cp -f translations/exo-terminal-emulator.desktop  /usr/share/applications/''')
            
        except :
            print(''' [ WARNING ] Echec de la maj de langue fichiers desktop ''')
                                    
        # pfiou "ca marche pas"
        #p1 = subprocess.Popen([ 'aptitude', 'versions', 'lightdm-gtk-greeter' ], stdout=subprocess.PIPE)
        #p2 = subprocess.Popen([ 'grep', '^i' ], stdin=p1.stdout, stdout=subprocess.PIPE)
        #p1.stdout.close()
        #p3 = subprocess.Popen([ 'grep', '-oe', '[1-9]\.[0-9]*\.[0-9]*-[0-9]*' , sdtin=p2.stdout, stdout=subprocess.PIPE)
        #p2.stdout.close()
        #output = p3.communicate()[0]
        
        # add some missing translation in advance
        os.system('aptitude install kde-l10n-fr menu-l10n qttranslations5-l10n qtcurve-l10n')
        
        if args.desktop == 'mate' :
            # fix traduction pour panneau de configuration
            os.system('cp -rf translations/mate-panel/*.desktop /usr/share/applications') #tmp fix for all users
            os.system('chown ' + logname + '.' + logname + 'translations/*.desktop') #local fix 
            os.system('cp -rf translations/mate-panel/*.desktop /home/' + logname + '/.local/share/applications/')
    
    #
    # DVD
    # 
    if args.dvd :
        try :
            os.system( '/usr/share/doc/libdvdread4/install-css.sh' )
        except :
            print( '''[ WARNING ] /usr/share/doc/libdvdread4/install-css.sh n'a pas pu etre execute''')
    
    #
    # Ejection lecteur dvd/cd / users-admin fail 
    #
    if args.patchs :
        if 'eject' in args.patchs :
            os.system('''cp -rf patchs/61-sr-change.rules /etc/udev/rules.d/61-sr-change.rules''')
            os.system('''cp -rf patchs/sr_change.sh       /usr/local/bin/sr_change.sh ''')
            os.system('''chmod +x                         /usr/local/bin/sr_change.sh ''')
        if 'users-admin' in args.patchs :
            os.system('''dpkg -i patchs/gnome-system_''' + str(Arch()) + '''.deb''')
            
    #
    # blacklist modules
    #
    def rmmod( module ) :
        module=module.strip()
        if not re.search( ' ', module ) :
            try :
                os.system('''
                    echo "blacklist '''+ module + '''" | tee /etc/modprobe.d/blacklist-''' + module + '''.conf
                    rmmod ''' + module + '''
                    update-initramfs -u''')
            except :
                print( '''[ WARNING ] echec du blacklistage du module ''' + module )

    if args.rmmod :
        for module in args.rmmod :
            rmmod( module )
                         
    #
    # Lightdm
    #
    if args.lightdm :
        for lightdm_desktop_conf in lightdm_xubuntu_conf, lightdm_ubuntu_conf :
            if os.path.exists ( lightdm_desktop_conf ) :
                # search if line already in there
                isLightNumlockxSet   = False
                isLightGuestSet      = False  
                isLightShowRemoteSet = False
                with open( lightdm_desktop_conf) as f :
                    for line in f.readlines() :
                        if re.search('''^greeter-setup-script=/usr/bin/numlockx''',  line) : isLightNumlockxSet   = True
                        if re.search('''^allow-guest=(false|true)'''               , line) : isLightGuestSet      = True
                        if re.search('''^greeter-show-remote-login=(false|true)''' , line) : isLightShowRemoteSet = True
                        
                    # Activation pave numerique                                
                    if 'numpad' in args.lightdm and os.path.exists( '/usr/bin/numlockx' ) :
                        if not isLightNumlockxSet :
                            os.system( '''echo '\ngreeter-setup-script=/usr/bin/numlockx on' >> ''' + lightdm_desktop_conf )
                        else :
                             os.system( '''sed -e 's/^greeter-setup-script=/usr/bin/numlockx .*/greeter-setup-script=/usr/bin/numlockx on/' -i ''' + lightdm_desktop_conf )
                        print( '''[ INFO ] active pad for lightdm in ''' + lightdm_desktop_conf )
                    
                    # Desactivation guest session
                    if ('noguest' in args.lightdm) :
                        if not isLightGuestSet :
                            os.system( '''echo '\nallow-guest=false' >> ''' + lightdm_desktop_conf )
                        else :
                            os.system( '''sed -e 's/^allow-guest=(false|true)/allow-guest=false/' -i ''' + lightdm_desktop_conf )
                            
                        print( '''[ INFO ] Desactivation session invite''' + lightdm_desktop_conf )
                    
                    # Desactivation login distant    
                    if not isLightShowRemoteSet    :
                        os.system( '''echo  '\ngreeter-show-remote-login=false' >> ''' + lightdm_desktop_conf )
                    else :
                        os.system( '''sed -e 's/^greeter-show-remote-login=(false|true)/greeter-show-remote-login=false/' -i ''' + lightdm_desktop_conf )
                        
                    print( '''[ INFO ] Desactivation login distant''' + lightdm_desktop_conf )

    #
    # light-locker
    #
    if args.light_locker == 'off' :
        os.system(''' chown -R ''' + logname + '.' + logname + ''' light-locker''')
        
        # pour tout le monde
        os.system(''' mkdir -p                     /etc/skel/.config/autostart ''')
        os.system(''' cp -r light-locker/*.desktop /etc/skel/.config/autostart''')

        # pour l'utilisateur actuel
        os.system(''' mkdir -p                       /home/''' + logname + '''/.config/autostart''')
        os.system(''' cp -pf light-locker/*.desktop  /home/''' + logname + '''/.config/autostart''')
        
    #
    # doc
    #
    if args.pdf_hello :
        if 'fr' in args.lang :
            try :
                os.system('''cp -pf doc/Bonjour.pdf /home/''' + logname + '''/Bureau''')
            except :
                print('[ ERREUR ] lors de la copie du pdf de bienvenue')

    #
    # APT recherche des mises tous les 
    #
    if args.apt :
        aptPeriodicUpdatePackageList = False
        aptPeriodicFile = '/etc/apt/apt.conf.d/10periodic'
        nbDaysUpdate = 7 
        try :
            with open( aptPeriodicFile ) as f :
                for line in f.readlines() :
                    if re.search('''^APT::Periodic::Update-Package-Lists "[0-9]*"'''         , line ) : aptPeriodicUpdatePackageList   = True
                    if re.search('''^APT::Periodic::Download-Upgradeable-Packages "[0-9]*"''', line ) : aptPeriodicDownloadUpgradeable = True
                    if re.search('''^APT::Periodic::Unattended-Upgrade "[0-9]*"'''           , line ) : aptPeriodicUnattendedUpgrade   = True
            
            if not aptPeriodicUpdatePackageList :
                # echo seul ne permet pas de modifier grr
                os.system('''echo '\nAPT::Periodic::Update-Package-Lists "''' + str(nbDaysUpdate) + '''";' | tee -a ''' + aptPeriodicFile )
            else :
                os.system('''sed -e 's/^APT::Periodic::Update-Package-Lists "[0-9]*";/APT::Periodic::Update-Package-Lists "''' + str(nbDaysUpdate) + '''";/' -i ''' + aptPeriodicFile )
                
            if not  aptPeriodicDownloadUpgradeable:
                # echo seul ne permet pas de modifier grr
                os.system('''echo '\nAPT::Periodic::Download-Upgradeable-Packages "''' + str(1) + '''";' | tee -a ''' + aptPeriodicFile )
            else :
                os.system('''sed -e 's/^APT::Periodic::Download-Upgradeable-Packages "[0-9]*";/APT::Periodic::Download-Upgradeable-Packages "''' + str(1) + '''";/' -i ''' + aptPeriodicFile )
                
            if not aptPeriodicUpdatePackageList :
                # echo seul ne permet pas de modifier grr
                os.system('''echo '\nAPT::Periodic::Update-Package-Lists "''' + str(1) + '''";' | tee -a ''' + aptPeriodicFile )
            else :
                os.system('''sed -e 's/^APT::Periodic::Unattended-Upgrade "[0-9]*";/APT::Periodic::Unattended-Upgrade "''' + str(1) + '''";/' -i ''' + aptPeriodicFile )
            
        except :
            print( '''[ WARNING ] Echec de la mise a jour du temps de rafraichissement de la liste des paquets ''')

    #
    # Firefox and Thunderbird
    #
    if os.path.exists( '/etc/firefox/' ) :
        os.system( ''' killall -9 firefox && sleep 5''')
        os.system( ''' cp -rf mozilla/syspref.js /etc/firefox/ ''' )
        os.system( ''' cp -rf mozilla/searchplugins/locale/fr/. /usr/lib/firefox/distribution/searchplugins/locale/fr ''')
        
        if 'fr' in args.lang :
            try :
                os.system(''' wget ''' + urlMOZextOrthographe + ''' -O fr-dicollecte@dictionaries.addons.mozilla.org.xpi  ''')
                
                # fr-classic fr-reform fr-classic-reform fr-modern
                if os.path.exists('/etc/firefox/syspref.js') :
                    os.system(''' echo "pref(spellchecker.dictionary, 'fr-modern');" >> /etc/firefox/syspref.js ''')

                os.system('''rm -rf fr-dicollecte@dictionaries.addons.mozilla.org''')
                os.system('''mkdir  fr-dicollecte@dictionaries.addons.mozilla.org''')
                os.system('''unzip  fr-dicollecte@dictionaries.addons.mozilla.org.xpi -d fr-dicollecte@dictionaries.addons.mozilla.org''')
                try :
                    os.system( ''' cp -rf fr-dicollecte@dictionaries.addons.mozilla.org  /usr/lib/firefox-addons/extensions ''')
                except :
                     print('''[ ERROR ] copying diacollecte extension ''')
                try :
                    os.system( ''' cp -r fr-dicollecte@dictionaries.addons.mozilla.org  /usr/lib/thunderbird-addons/extensions ''')
                except :   
                    print('''[ ERROR ] copying diacollecte extension ''')
            except :
                print('''[ ERROR ] getting diacollecte extension for mozilla ''')  
        
        
        #os.system( ''' rm -rf /usr/share/mozilla/extensions/\{ec8030f7-c20a-464f-9b0e-13a3a9e97384\}''') ubufox remove by apt
        for searchplugin in searchpluginsBlacklist :
            os.system( ''' rm -rf /usr/lib/firefox/distribution/searchplugins/locale/*/'''     + searchplugin + '''*.xml ''')
            os.system( ''' rm -rf /usr/lib/thunderbird/distribution/searchplugins/locale/*/''' + searchplugin + '''*.xml ''')
        
        if 'ALL'            in args.xpi :
            os.system( ''' cp -r XPI/*.xpi /usr/lib/firefox-addons/extensions ''')
        else :
            if 'adb'            in args.xpi :
                ## adblock edge
                #os.system( ''' cp -r XPI/\{fe272bd1-5f76-4ea4-8501-a05d35d823fc\}.xpi /usr/lib/firefox-addons/extensions ''')
                #pref("extensions.adblockedge.patternsfile", "/etc/firefox/abe_patterns.ini" )
                #os.system( ''' cp    XPI/patterns.ini /etc/firefox/abe_patterns.ini ''') #force patterns.ini
                
                ## adblock plus
                os.system( ''' cp -r XPI/\{d10d0bf8-f5b5-c8b4-a8b2-2b9879e08c5d\}.xpi /usr/lib/firefox-addons/extensions ''')

            if 'httpsEverywhere' in args.xpi :
                os.system( ''' cp -r XPI/https-everywhere@eff.org.xpi                 /usr/lib/firefox-addons/extensions ''')

            if 'NoScript'        in args.xpi :
                os.system( ''' cp -r XPI/\{73a6fe31-595d-460b-a920-fcc0f8843232\}.xpi /usr/lib/firefox-addons/extensions ''')

        os.system( ''' rm -rf /home/*/.mozilla ''' )
        
        # firefox addons
        # /usr/lib/firefox-addons/extensions/{id}.xpi
        # browser.search.defaultenginename
        # browser.search.selectedEngine
        print( ''' [ INFO ] Firefox customisation ''' )

    #
    # Libreoffice
    #
    #urlLOextOrthographe='http://www.dicollecte.org/grammalecte/oxt/Grammalecte-v0.4.1.oxt'
    if 'fr' in args.lang and ( 'orthographe' in args.libreoffice ) :
        try :
            #os.system('''aptitude install -y libreoffice-java-common default-jre''')
            os.system('''wget ''' + urlLOextOrthographe + ''' -O LOextOrthographe.oxt''')
            os.system('''unopkg add --shared LOextOrthographe.oxt''')
            os.system('''rm -rf /home/''' + logname  + '''/.config/libreoffice ''' ) # fix probleme de droit pour les fichiers de configuration libreoffice
        except :
            print('''[ ERROR ] during wget/installation Libreoffice extension ''')

    #
    # Apport ( bug reports )
    #
    if os.path.exists( '/etc/default/apport' ) :
        os.system( ''' sed -e 's/enabled=1/enabled=0/' -i /etc/default/apport ''')
        os.system( ''' rm -rf /var/crash/* ''' )
        os.system( ''' restart apport ''')
        print( ''' [ INFO ] bug reports disabled ''' )

    #
    # Firewall
    #
    os.system( ''' ufw enable ''')

    #
    # themes
    #
    # on force la suppression pour mieux reinstaller
    if args.desktop :
        os.system('''[ INFO ] Suppression des installation precedente des icones faenza''')
        os.system('''rm -rf /home/*/.icons/Faenza*''')
        os.system('''rm -rf /usr/share/icons/Faenza*''')

    #  Icons
        os.system( 
'''faenza/INSTALL<<EOF
Y
U
M
Y
D
EOF
''')

        path='/usr/share/icons/Faenza'
        if os.path.exists(path) :
            # maj icone pour gtk-theme-config 
            os.system('''ln -rsf ''' + path + '''/categories/scalable/applications-graphics.svg ''' + path + '''/apps/scalable/gtk-theme-config.svg''')
            # maj icone pour mugshot
            os.system('''ln -rsf ''' + path + '''/emblems/scalable/emblem-personal.svg '''          + path + '''/apps/scalable/mugshot.svg''')
            # maj icone pour menulibre
            os.system('''ln -rsf ''' + path + '''/apps/scalable/menu-editor.svg '''                 + path + '''/apps/scalable/menulibre.svg''')
        
            # maj icones
            os.system('''cp -rfv apps/. ''' + path + '''/apps/scalable/''')

        # maj cache icons
        if os.path.exists('/usr/share/icons/Faenza-Darkest' ) :
            os.system('gtk-update-icon-cache /usr/share/icons/Faenza')
            os.system('gtk-update-icon-cache /usr/share/icons/Faenza-Dark')
            os.system('gtk-update-icon-cache /usr/share/icons/Faenza-Darker')
            os.system('gtk-update-icon-cache /usr/share/icons/Faenza-Darkest')
            os.system('gtk-update-icon-cache /usr/share/icons/Faenza-Radiance')
            os.system('gtk-update-icon-cache /usr/share/icons/Faenza-Amdiance')

    #
    # themes pour xfce
    #
    if args.desktop == 'xfce' :
        
        #   
        # Plymouth ecran de demarrage
        #
        try :
            os.system('cp -rf wallpaper_plymouth.png /lib/plymouth/themes/xubuntu-logo/wallpaper.png')
        except :
            print('[ ERROR ] echec du changement de fond pour ecran de demarrage plymouth')
        
        #
        # Change theme in root directories  
        #   
        varDir        = '/etc/xdg/xdg-xubuntu/xfce4/xfconf/xfce-perchannel-xml/'
        xfwm4         = varDir + 'xfwm4.xml'
        xsettings     = varDir + 'xsettings.xml'
        xfce4_notifyd = varDir + 'xfce4-notifyd.xml'
        xfce4_desktop = varDir + 'xfce4-desktop.xml'

        os.system('''
            cp -rf ''' + xfwm4         + ' ' + xfwm4         + '''.bck
            cp -rf ''' + xsettings     + ' ' + xsettings     + '''.bck
            cp -rf ''' + xfce4_notifyd + ' ' + xfce4_notifyd + '''.bck
            cp -rf ''' + xfce4_desktop + ' ' + xfce4_desktop + '''.bck
        ''')

        os.system('mkdir -p /etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml')
        os.system('mkdir -p /etc/skel/.config/xfce4/panel')
        
        # Regeneration du tableau de bord
        if os.path.exists('panels.sh') and args.panel :
            # execution avec l'utilisateur local de la regeneration des tableaux de bords
            # Problemes rencontres :
            #    - version sudo -iu user :
            #        certains processus sont mal "recables"
            #.system('''sudo -iu ''' + logname + '''\
            #             whoami;\
            #            ./panels.sh 3;\
            #            exit ''')
            #    - version root :
            #        cree des fichier root non accessibles / problemes de droits
            #
            # 

            # Nouvelle version un peu bricolo 
            #     un premier processus est lance avec les droits utilisateur normaux
            #     et attend la creation du fichier beginWaitPanels qui declenche
            #     la creation des nouveaux tableaux de bord
            #     l'execution du script root est stoppe pendant ce temps
            #     un failsafe a 10 min est cree en cas d'echec des panels.sh
            os.system('''
                touch beginWaitPanels 
                tsleep=0
                while [ ! -f endWaitPanels ] && [ $tsleep -le 600 ]
                do
                    sleep 1
                    tsleep=$(($tsleep + 1))
                done
                
                if [ $tsleep -ge 600 ]
                then
                    rm -rf /home/*/.config/xfce4/panel/launcher*
                    rm -rf /home/*/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
                fi
            ''' )
            time.sleep(10)
        
        # logos
        os.system('cp -rvf pixmaps/. /usr/share/pixmaps')
        os.system('cp -rvf apps/.    /usr/share/pixmaps')
        
        # whisker menu config
        os.system('cp -rvf tableau_de_bord/whiskermenu-1.rc /etc/skel/.config/xfce4/panel')
        os.system('cp -rvf pixmaps/xubuntu-logo-custom.svg /usr/share/pixmaps')
 
        # Theme
        if os.path.exists(xfwm4) and os.path.exists(xfce4_notifyd) :
            print('[ INFO ] modification ' + xfce4_notifyd + ' ' + xfwm4 )
            os.system('''sed -e 's&<property name="theme" type="string" value="[a-zA-Z0-9_-]*"/>&<property name="theme" type="string" value="Numix"/>&' -i ''' + xfwm4 + ' ' + xfce4_notifyd)
        
        # Icons    
        if os.path.exists(xsettings) :
            print('[ INFO ] modification ' + xsettings )
            os.system('''sed -e 's&<property name="ThemeName" type="string" value="[a-zA-Z0-9_-]*"/>&<property name="ThemeName" type="string" value="Greybird"/>&' -i ''' + xsettings)

            # maj cache icons
            if os.path.exists('/usr/share/icons/Faenza-Darkest' ) :
                os.system('''sed -e 's&<property name="IconThemeName" type="string" value="[a-zA-Z0-9_-]*"/>&<property name="IconThemeName" type="string" value="Faenza-Darkest"/>&' -i ''' + xsettings)
        
        # Wallpaper
        wallpaperPath='/usr/share/xfce4/backdrops/'
        if os.path.exists(wallpaperPath) and os.path.exists('wallpaper.png') :
            print('[ INFO ] modification du background ' )
            os.system('''cp -rf wallpaper.png ''' + wallpaperPath)
            if os.path.exists(wallpaperPath + 'wallpaper.png'):
                os.system(''' sed -e 's&<property name="image-path" type="string" value="[a-zA-Z0-9_-.]*"/>&<property name="image-path" type="string" value="''' + wallpaperPath + '''wallpaper.png"/>&' -i ''' + xfce4_desktop)
        
        # Panel
        if args.panel and os.path.exists( '/home/' + logname + '/.config/xfce4/panel') and  os.path.exists( '/home/' + logname + '/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml'):
            print('[ INFO ] modification des tabeaux de bord')
            os.system('echo ' + logname)
            os.system('cp -rvf /home/' + logname + '/.config/xfce4/panel                                      /etc/skel/.config/xfce4')
            os.system('cp -rvf /home/' + logname + '/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml /etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml')
        
        # change logo of whisker menu
        if args.logo :
            logo=args.logo.strip()
            if os.path.exists('pixmaps/' + logo) :
                os.system('cp pixmaps/' + logo + ' /usr/share/pixmaps/')
                #if os.path.exists('/usr/share/pixmaps/' +  logo) and os.path.exists('/usr/share/xfce4/whiskermenu/defaults.rc') :
                if os.path.exists('/usr/share/pixmaps/' +  logo) and os.path.exists('/etc/skel/.config/xfce4/panel/whiskermenu-1.rc') :
                    os.system('''sed -e 's&^button-icon=.*&button-icon=/usr/share/pixmaps/''' + logo + '''&' -i  /etc/skel/.config/xfce4/panel/whiskermenu-1.rc''')
                    os.system('''rm -rvf /home/*/.config/xfce4/panel/whiskermenu*.rc''')
                    
        # Force recopie du whisker menu modifie
        if os.path.exists('/home/' + logname ) :
                os.system('''
                    cp -rvf /etc/skel/.config/xfce4/panel/whiskermenu-1.rc /home/''' + logname + '''/.config/xfce4/panel/
                    chown ''' + logname + '.' + logname + ' /home/' + logname + '/.config/xfce4/panel/'
                )
        # force regeneration of configuration
        #os.system('''rm -rf /home/*/.config/xfce4''')
        for f in 'xfwm4.xml', 'xsettings.xml',  'xfce4-notifyd.xml', 'xfce4-desktop.xml' :
            os.system('''rm -rvf /home/*/.config/xfce4/xfce-perchannel-xml/''' + f)
 
        # force xfce4-panel -r
        #os.system('''sudo -iu ''' + logname + '''\
        #                 whoami;\
        #                xfce4-panel -&;\
        #                exit ''')           

        # default softwares
        try :
            os.system(''' cp -rf /home/''' + logname + '''/.config/xfce4/xfconf/xfce-perchannel-xml/thunar-volman.xml /etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml''')
        except :
            print( '[ ERROR ] lors de la copie de .config/xfce4/xfconf/xfce-perchannel-xml/thunar-volman.xml dans /etc/skel ')

    #
    # Special configurations
    # 
    if args.desktop == 'mate' :
        os.system('cp -f linuxmint/rappel_maj.sh /usr/local/bin' )
        os.system('chmod +x /usr/local/bin/rappel_maj.sh')
        os.system('cp -f linuxmint/rappel_man.desktop /home/' + logname + '/.config/autostart')
        os.system('chown ' + logname +  '.' + logname + ' /home/' + logname + '/.config/autostart/rappel_maj.sh')
        os.system('chmod 644 /home/' + logname + '/.config/autostart/rappel_maj.sh')
    elif args.desktop == 'xfce' :
        # desktop show home and removable devices
        if os.path.exists(xfce4_desktop) :
            print('[ INFO ] modification ' + xfce4_desktop )
            os.system('''sed -e 's&<property name="show\(-\(home\|trash\|filesystem\|\(\(\(unknown\|network\|device\)-\|\)removable\)\)\)" type="bool" value="[a-zA-Z0-9_-]*"/>&<property name="show\\1" type="bool" value="false"/>&' -i ''' + xfce4_desktop )
            os.system('''sed -e 's&<property name="show\(-\(home\|\(\(\(unknown\|network\|device\)-\|\)removable\)\)\)" type="bool" value="[a-zA-Z0-9_-]*"/>&<property name="show\\1" type="bool" value="true"/>&' -i ''' + xfce4_desktop )
            
        # non root commands just to remember    
        #os.system( 'xfconf-query --channel  xsettings --property "/Net/IconThemeName" --set Faenza-Darkest')
        #os.system( 'xfconf-query --channel xfwm4         --property "/general/theme" -s  Numix')
        #os.system( 'xfconf-query --channel xsettings     --property "/Net/ThemeName" -s  Numix')
        #os.system( 'xfconf-query --channel xfce4-notifyd --property "/theme"         -s  Numix')
        
        # desktop show icons
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-home"               -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-filesystem"         -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-trash"              -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-unknown-removable"  -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-removable"          -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-network-removable"  -s false')
        #os.system( 'xfconf-query --channel xfce4-desktop --property "/desktop-icons/file-icons/show-device-removable"   -s false')

        # Disable save session
        if args.no_session_save :
            os.system('''rm -rf /home/*/.cache/sessions/* ''')
            os.system('''mkdir /etc/xdg/xfce4/kiosk''')
            if not os.path.exists( '/etc/xdg/xfce4/kiosk/kioskrc' ) :
                os.system('''echo '[xfce4-session]\nSaveSession=NONE' >> /etc/xdg/xfce4/kiosk/kioskrc''')
            else :
                print('''[ WARNING ] /etc/xdg/xfce4/kioskrc existe deja ''')
                

                
    elif args.desktop == 'unity' :
        os.system('''aptitude install -y unity-tweak-tool''')
        
        #garder les scopes de bases permettant l'utilisation du dash, virer tous ceux "publicitaires"
        if args.fixubuntu :
            os.system('''aptitude remove -y --purge -P ~n^unity-scope unity-scope-home= unity-scopes-master-default= unity-lens-friends unity-lens-shopping''')
            
            os.system('''rm -rf /usr/share/unity/scopes/*/flickr.scope  /usr/share/unity/scopes/*/facebook.scope''')
            os.system('''rm -rf /usr/share/unity-scopes/facebook /usr/share/unity-scopes/flickr''')
            
            os.system('''rm -rf /usr/share/applications/ubuntu-amazon-default.desktop''')

        #change background
        ##os.system('''mv /usr/share/backgrounds/warty-final-ubuntu.png /usr/share/backgrounds/warty-final-ubuntu-ori.png''')
        ##os.system('''mv -f wallpaper.png /usr/share/backgrounds/warty-final-ubuntu-ori.png''')
        
    #
    # forcage de mise a jour des langues
    #
    try :
        os.system('aptitude update')
        print( '[ INFO ] recherche du support de langue '    + ' '.join(args.lang) + '+en')
        # prise en charge des langues deja installes
        langSupport = os.popen( 'check-language-support').read() 
        os.system (' aptitude install -y ' + langSupport )     
        # ajout d'autres langues
        for lang in args.lang :
            try :
                langSupport = os.popen( 'check-language-support  -l ' + lang ).read()
                os.system (' aptitude install -y ' + langSupport )
            except :
                print( '[ ERREUR ] lors de la mise a jour de la langue :' + lang )
        
    except :
        print( '[ ERREUR ] lors de la mise a jour des langues' )

    #
    # Optimisations
    #
    if args.swap :
        os.system( './swappiness.sh ' + str(args.swap) )

    #
    # for everyone
    #
    os.system('''rm -rf /home/*/.cache/sessions/* ''')
    os.system('''xdg-user-dirs-update''')
    os.system('''xdg-user-dirs-gtk-update''')

    #
    # Pilotes proprio
    #
    try :
        os.system( '''/usr/bin/python3 /usr/bin/software-properties-gtk --open-tab=4''')
    except :
        print('''[ WARNING ] could not open software-preperties to see if proprietary software are available''')
