#!/bin/bash
tmp_dir=teamviewerqs_tmp

rm -rf ${tmp_dir}
mkdir ${tmp_dir}
# get archive
wget download.teamviewer.com/download/teamviewer_qs.tar.gz -O teamviewer_qs.tar.gz

# extract
tar zxvf teamviewer_qs.tar.gz -C ${tmp_dir} --strip-components=1

# get version
teamviewer_version=`ls -1 ${tmp_dir}/tv_bin/desktop/teamviewer-teamviewer*.desktop | sed -e 's/.*teamviewer//' -e 's/\.desktop//'`

#
install_dir=/opt/teamviewer${teamviewer_version}_qs
rm -rf ${install_dir}
mv ${tmp_dir} ${install_dir}

# set permissions
addgroup teamviewer
chown root.teamviewer ${install_dir} -R
chmod g+w ${install_dir} -R

# Modify launcher
launcher_dir=${install_dir}/tv_bin/desktop
sed -e "s/^Name=.*/Name = Teamviewer ${teamvier_version}/" \
    -e "s&^Exec=.*&Exec=${install_dir}/teamviewer&"        \
    -e "s&^Icon=.*&Icon=teamviewer.png&"                   \
    -i  ${launcher_dir}/teamviewer*.desktop

chmod +x ${launcher_dir}/teamviewer*.desktop
cp ${launcher_dir}/teamviewer*.desktop /usr/share/applications/teamviewer_${teamviewer_version}_qs.desktop

echo '[ INFO ] Si le script est lancé manuellement exécutez la commande suivante 
afin de pouvoir utiliser teamviewer:
usermod -aG teamviewer VOTRE_LOGIN'
exit 0
