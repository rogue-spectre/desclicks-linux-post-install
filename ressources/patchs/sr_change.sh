#!/bin/sh

if [ $# -eq 1 ]
then
    source=/dev/sr$1
else
    source=/dev/sr0
fi

dvd+rw-mediainfo $source > /dev/null

n=$?
if [ "$n" -eq '251' ]
then 
    umount $source
fi
exit 0
